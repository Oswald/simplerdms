﻿using SimpleRDMS.Compiler.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Entities
{
    public class QueryPart
    {
        public QueryPartType Type { get; }
        public List<QueryPart> Subparts { get; }
        public Dictionary<QueryPropertyValue, string> Properties { get; } 

        public QueryPart(QueryPartType type)
        {
            Type = type;
            Subparts = new List<QueryPart>();
            Properties = new Dictionary<QueryPropertyValue, string>();
        }
    }
}
