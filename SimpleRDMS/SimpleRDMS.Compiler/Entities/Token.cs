﻿using SimpleRDMS.Compiler.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Entities
{
    public class Token
    {
        public TokenType Type { get; set; }
        public string Content { get; set; }
    }
}
