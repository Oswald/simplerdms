﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Entities
{
    public class RequiredToken
    {
        public List<string> AcceptedContent { get; }
        public bool AddToProperties { get; }
        public QueryPropertyValue? Property { get; }
        public TokenType? AcceptedTokenType { get; }
        public bool Optional { get; }
        public RequiredToken RequiredTrailingToken { get; }
        public RequiredToken(string acceptedContent, bool optional = false, RequiredToken requiredTrailingToken = null, QueryPropertyValue? property = null, TokenType? acceptedTokenType = null)
        {
            AcceptedContent = new List<string> { acceptedContent };
            AddToProperties = property != null;
            Property = property;
            RequiredTrailingToken = requiredTrailingToken;
            Optional = optional;
            AcceptedTokenType = acceptedTokenType;
        }

        public RequiredToken(QueryPropertyValue property, TokenType acceptedTokenType, List<string> acceptedContent = null, bool optional = false, RequiredToken requiredTrailingToken = null)
        {
            AcceptedContent = acceptedContent;
            AddToProperties = true;
            Property = property;
            AcceptedTokenType = acceptedTokenType;
            RequiredTrailingToken = requiredTrailingToken;
            Optional = optional;
        }

        public override string ToString()
        {
            if (AcceptedTokenType != null && Property != null && AcceptedTokenType == TokenType.Symbol)
            {
                return $"Expected: {Property}";
            }
            else
            {
                var sb = new StringBuilder();

                if (AcceptedTokenType != null)
                {
                    sb.Append($"Accepted token type: {AcceptedTokenType}. ");
                }
                if (AcceptedContent != null)
                {
                    sb.Append("Accepted token content: ");
                    foreach (var content in AcceptedContent)
                    {
                        sb.Append(content + " ");
                    }
                }

                return sb.ToString();
            }
        }
    }
}
