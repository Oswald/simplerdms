﻿using SimpleRDMS.Compiler.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Entities
{
    public class TableRule
    {
        public TableRuleType Name { get; set; }
        public List<string> Columns { get; set; }
    }
}
