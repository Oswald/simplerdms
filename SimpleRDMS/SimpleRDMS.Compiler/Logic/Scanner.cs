﻿using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Compiler.Logic
{
    public class Scanner
    {
        string Input { get; }
        int Index { get; set; }
        string CurrentWord { get; set; } = "";
        public Token CurrentToken { get; private set; }

        static readonly HashSet<string> SpecialCharacters = new HashSet<string>
        {
            ",",
            "(",
            ")",
            "=",
            "!=",
            "!",
            "*"
        };

        static readonly HashSet<char> CharactersAllowedInSymbols = new HashSet<char>
        {
            '_',
            '.'
        };

        static readonly HashSet<string> Keywords = new HashSet<string>
        {
            "CREATE",
            "TABLE",
            "SELECT",
            "WHERE",
            "REMOVE",
            "UPDATE",
            "INTEGER",
            "STRING",
            "UNIQUE",
            "NOT",
            "NULL",
            "AND",
            "OR",
            "BEGIN",
            "TRANSACTION",
            "COMMIT",
            "ROLLBACK"
        };

        public Scanner(string input)
        {
            Input = input;
            MoveToNextToken();
        }

        public bool CurrentWordIsOpenString()
        {
            return CurrentWord.Count() != 0 && (CurrentWord[0] == '"' && CurrentWord.Last() != '"');
        }

        public void MoveToNextToken()
        {
            SetIndexToNextValidCharacter();

            var character = ReadNextCharacter();

            while (character != null)
            {
                if (CurrentWordIsOpenString())
                {
                    CurrentWord += character.Value;
                }
                else if (char.IsWhiteSpace(character.Value))
                {
                    CurrentToken = ParseCurrentWord();
                    return;
                }
                else if (CanBeAddedToWord(character.Value))
                {
                    CurrentWord += character.Value;
                }
                else
                {
                    var token = ParseCurrentWord();
                    CurrentWord = character.Value.ToString();
                    CurrentToken = token;
                    return;
                }

                character = ReadNextCharacter();
            }

            CurrentToken = ParseCurrentWord();
        }

        bool CanBeAddedToWord(char c)
        {
            if(CurrentWord == "")
            {
                return true;
            }
            else if(CurrentWord[0] == '"' && CurrentWord.Last() != '"') //Anything can be added to string
            {
                return true;
            }
            else if (SpecialCharacters.Contains(CurrentWord))
            {
                if (SpecialCharacters.Contains(CurrentWord + c))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if(char.IsLetterOrDigit(c) || CharactersAllowedInSymbols.Contains(c))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        void SetIndexToNextValidCharacter()
        {
            while(Index < Input.Length && char.IsWhiteSpace(Input[Index]))
            {
                Index += 1;
            }
        }

        char? ReadNextCharacter()
        {
            if(Index < Input.Length)
            {
                var c = Input[Index];
                Index += 1;
                return c;
            }
            else
            {
                return null;
            }
        }

        Token ParseCurrentWord()
        {
            var token = new Token
            {
                Type = ParseTokenType(),
                Content = CurrentWord
            };

            CurrentWord = "";

            return token;            
        }

        TokenType ParseTokenType()
        {
            if(CurrentWord == "")
            {
                return TokenType.EndOfString;
            }
            else if(SpecialCharacters.Contains(CurrentWord))
            {
                return TokenType.SpecialCharacter;
            }
            else if(Keywords.Contains(CurrentWord.ToUpper()))
            {
                return TokenType.Keyword;
            }
            else if(CurrentWord[0] == '"' && CurrentWord.Last() == '"')
            {
                CurrentWord = CurrentWord.Trim(new char[] { '"' });
                return TokenType.String;
            }
            else if (int.TryParse(CurrentWord, System.Globalization.NumberStyles.Integer, CultureInfo.InvariantCulture, out int result))
            {
                return TokenType.Integer;
            }
            else if(char.IsLetter(CurrentWord[0]) && CurrentWord.All(c => char.IsLetterOrDigit(c) || CharactersAllowedInSymbols.Contains(c)))
            {
                //TODO
                return TokenType.Symbol;
            }
            else
            {
                throw new SimpleRDMSException($"Invalid input. Cannot parse word {CurrentWord}");
            }
        }
    }
}
