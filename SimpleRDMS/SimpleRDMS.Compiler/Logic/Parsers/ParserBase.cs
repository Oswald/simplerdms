﻿using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Compiler.Logic.Parsers
{
    public class ParserBase
    {
        public string ParserName { get; }
        public List<List<ParserBase>> ChildParsers { get; }
        
        public QueryPartType ParserType { get; }
        public List<RequiredToken> TokensBeforeParsers { get; }
        public List<RequiredToken> TokensAfterParsers { get; }
        public int[] MandatoryParsers { get; set; } = new int[] { 0 }; //Parsers that cannot be skipped
    bool BreakAfterFirstParser { get; set; }

        public List<RequiredToken> DelimiterTokens { get; set; } = new List<RequiredToken> { new RequiredToken(",") };


        public ParserBase(string name, QueryPartType parserType, List<RequiredToken> tokensBeforeParsers, List<ParserBase> parsers = null, List<RequiredToken> tokensAfterParsers = null)
        {
            ParserName = name;
            ParserType = parserType;
            ChildParsers = parsers == null ? new List<List<ParserBase>>() :  new List<List<ParserBase>> { parsers };
            TokensAfterParsers = tokensAfterParsers ?? new List<RequiredToken>();
            TokensBeforeParsers = tokensBeforeParsers ?? new List<RequiredToken>();
        }

        public ParserBase(string name, QueryPartType parserType, List<RequiredToken> tokensBeforeParsers, List<List<ParserBase>> parsers, List<RequiredToken> tokensAfterParsers = null)
        {
            ParserName = name;
            ParserType = parserType;
            ChildParsers = parsers;
            TokensAfterParsers = tokensAfterParsers ?? new List<RequiredToken>();
            TokensBeforeParsers = tokensBeforeParsers ?? new List<RequiredToken>();
        }

        bool RequiredTokenMatches(Scanner scanner, RequiredToken token)
        {
            return (token.AcceptedContent == null || token.AcceptedContent.Any(word => word.Equals(scanner.CurrentToken.Content, StringComparison.OrdinalIgnoreCase))) &&
                   (token.AcceptedTokenType == null || token.AcceptedTokenType == scanner.CurrentToken.Type);
        }

        public bool Accepts(Scanner scanner)
        {
            if (TokensBeforeParsers.Count != 0)
            {
                foreach (var token in TokensBeforeParsers)  //Check if there are optional tokens
                {
                    if (!token.Optional)
                    {
                        return RequiredTokenMatches(scanner, token);
                    }
                    else
                    {
                        if(RequiredTokenMatches(scanner, token))
                        {
                            return true;
                        }
                    }
                }
            }
            else if (ChildParsers.Count > 0)
            {
                return ChildParsers.First().Any(p => p.Accepts(scanner));
            }

            return false;

        }

        /// <summary>
        /// If called, accepts only one parser, instead of looping through until there is no acceptable input
        /// </summary>
        /// <returns></returns>
        public ParserBase AcceptOnlyOneParserPerGroup()
        {
            BreakAfterFirstParser = true;
            return this;
        }

        /// <summary>
        /// Sets parsers on parser lists, that are mandatory. Defaults to 0. For example, in this: SELECT stuff WHERE condition, stuff (0) is mandatory, but where (1) is optional.
        /// </summary>
        /// <returns></returns>
        public ParserBase WithMandatoryParsers(int[] parsers)
        {
            MandatoryParsers = parsers;
            return this;
        }

        public ParserBase SetDelimiters(List<RequiredToken> tokens)
        {
            DelimiterTokens = tokens;
            return this;
        }

        public QueryPart ParseToken(Scanner scanner)
        {
            var part = new QueryPart(ParserType);

            try
            {

                HandleRequiredTokens(TokensBeforeParsers, part, scanner);

                ParserBase parser = null;

                if (ChildParsers != null)
                {
                    foreach (var parserGroup in ChildParsers)
                    {
                        while (true)
                        {
                            parser = parserGroup.FirstOrDefault(p => p.Accepts(scanner));
                            if (parser != null)
                            {
                                part.Subparts.Add(parser.ParseToken(scanner));

                                if (BreakAfterFirstParser)
                                {
                                    break;
                                }
                                else if (DelimiterTokens.Any(t => RequiredTokenMatches(scanner, t)))
                                {
                                    HandleToken(DelimiterTokens.First(t => RequiredTokenMatches(scanner, t)), scanner, part, true);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else if(!MandatoryParsers.Contains(ChildParsers.IndexOf(parserGroup))) //Optional parsers can be skipped. For example, there may not be WHERE after select
                            {
                                break;
                            }
                            else
                            {
                                //TODO: Print all possible parsers
                                string possibleTokens = "";

                                foreach(var possibleParser in parserGroup)
                                {
                                    possibleTokens += $"\r\n{possibleParser.ParserName} ({possibleParser.TokensBeforeParsers.First()})";
                                }

                                throw new SimpleRDMSException($"Could not parse token: {scanner.CurrentToken.Content} ({scanner.CurrentToken.Type}). Checked following parsers: {possibleTokens}");
                            }


                        }
                    }
                }

                HandleRequiredTokens(TokensAfterParsers, part, scanner);

            } 
            catch (SimpleRDMSException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException($"Error when parsing input with parser {ParserName}", e);
            }

            return part;
        }

        /// <summary>
        /// Delimiters cannot be added to properties, as there could be multiple items with the same key. Thus, delimiters are added to subparts.
        /// </summary>
        void HandleRequiredTokens(List<RequiredToken> tokens, QueryPart part, Scanner scanner, bool isDelimiterToken = false)  
        {
            foreach (var token in tokens)
            {
                HandleToken(token, scanner, part, isDelimiterToken);
            }
        }

        void HandleToken(RequiredToken token, Scanner scanner, QueryPart part, bool delimiter)
        {
            if (RequiredTokenMatches(scanner, token))
            {
                if (token.AddToProperties)
                {
                    if (delimiter)
                    {
                        var delimiterSubpart = new QueryPart(QueryPartType.Delimiter);
                        delimiterSubpart.Properties.Add(token.Property.Value, scanner.CurrentToken.Content);

                        part.Subparts.Add(delimiterSubpart);
                    }
                    else
                    {
                        part.Properties.Add(token.Property.Value, scanner.CurrentToken.Content);
                    }
                }
                scanner.MoveToNextToken();

                if (token.RequiredTrailingToken != null)
                {
                    HandleToken(token.RequiredTrailingToken, scanner, part, delimiter);
                }
            }
            else if (!token.Optional)
            {
                throw new SimpleRDMSException($"Scanned token {scanner.CurrentToken.Content} (type: {scanner.CurrentToken.Type}) did not match required token. [{token}]");
            }
        }

    }
}
