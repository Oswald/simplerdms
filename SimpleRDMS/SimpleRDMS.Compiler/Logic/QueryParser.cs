﻿using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic.Parsers;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Logic
{
    public static class QueryParser
    {
        static ParserBase ParserBase { get; } = CreateParser();

        static ParserBase CreateParser()
        {
            var columnParser = new ParserBase("ColumnListParser", QueryPartType.ColumnName,
            new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
            }
            );

            var numberParser = new ParserBase("Number parser", QueryPartType.Integer,
            new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.Integer, TokenType.Integer)
            });

            var nullParser = new ParserBase("Null parser", QueryPartType.Null,
            new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.Null, TokenType.Keyword, new List<string> { "NULL" })
            });

            var stringParser = new ParserBase("String parser", QueryPartType.String,
            new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.String, TokenType.String)
            });

            var symbolParser = new ParserBase("Symbol parser", QueryPartType.ColumnName,
            new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
            });

            var notNull = new RequiredToken("NOT", true, new RequiredToken("NULL"), QueryPropertyValue.NotNullable);

            var conditionParsers = new List<ParserBase>
            {
                new ParserBase("Symbol OPERATOR Any", QueryPartType.Condition,
                new List<RequiredToken>
                {
                    new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol),
                    new RequiredToken(QueryPropertyValue.Operator, TokenType.SpecialCharacter, new List<string> { "!=", "=" }),
                },
                new List<ParserBase>
                {
                    symbolParser,
                    stringParser,
                    numberParser,
                    nullParser

                }).AcceptOnlyOneParserPerGroup(),
                new ParserBase("Integer OPERATOR Symbol", QueryPartType.Condition,
                new List<RequiredToken>
                {
                    new RequiredToken(QueryPropertyValue.Integer, TokenType.Integer),
                    new RequiredToken(QueryPropertyValue.Operator, TokenType.SpecialCharacter, new List<string> { "!=", "=" }),
                    new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
                }),
                new ParserBase("String OPERATOR Symbol", QueryPartType.Condition,
                new List<RequiredToken>
                {
                    new RequiredToken(QueryPropertyValue.String, TokenType.String),
                    new RequiredToken(QueryPropertyValue.Operator, TokenType.SpecialCharacter, new List<string> { "!=", "=" }),
                    new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
                })
            };

            conditionParsers.Add(new ParserBase("ConditionBracket", QueryPartType.ConditionBracket,
                new List<RequiredToken>
                {
                    new RequiredToken("(")
                },
                conditionParsers,
                new List<RequiredToken>
                {
                    new RequiredToken(")")
                })
                .SetDelimiters(new List<RequiredToken>
                {
                    new RequiredToken(QueryPropertyValue.LogicalConnectiveType, TokenType.Keyword, new List<string> { "AND", "OR" })
                })
            );

            var whereParser = new ParserBase("Where statement", QueryPartType.Where,
            new List<RequiredToken>
            {
                new RequiredToken("WHERE"),
            },
                conditionParsers
            ).SetDelimiters(new List<RequiredToken>
            {
                new RequiredToken(QueryPropertyValue.LogicalConnectiveType, TokenType.Keyword, new List<string> { "AND", "OR" })
            });

            var defineRule = new ParserBase("Define rule", QueryPartType.RuleDefinition,
                new List<RequiredToken>
                {
                    new RequiredToken("CONSTRAINT", false, new RequiredToken(QueryPropertyValue.RuleName, TokenType.Symbol)),
                    new RequiredToken(QueryPropertyValue.RuleType, TokenType.Keyword, new List<string> { "UNIQUE" }),
                    new RequiredToken("("),
                },
                new List<ParserBase>
                {
                    columnParser
                },
                new List<RequiredToken>
                {
                    new RequiredToken(")")
                }
            );

            var tableParser = new ParserBase("Create table", Enums.QueryPartType.CreateTable,
                new List<RequiredToken>
                {
                    new RequiredToken("CREATE"),
                    new RequiredToken("TABLE"),
                    new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol),
                    new RequiredToken("(")
                },
                new List<ParserBase>
                {
                    new ParserBase("Define column", QueryPartType.ColumnDefinition,
                    new List<RequiredToken>
                    {
                        new RequiredToken(QueryPropertyValue.DataType, TokenType.Keyword, new List<string> { "INTEGER", "STRING" }),
                        new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol),
                        notNull
                    }
                    ),
                    defineRule
                },
                new List<RequiredToken>
                {
                    new RequiredToken(")"),
                    new RequiredToken("")
                });

            var alterTableParser = new ParserBase("Alter table", QueryPartType.AlterTable,
                new List<RequiredToken>
                {
                    new RequiredToken("ALTER"),
                    new RequiredToken("TABLE"),
                    new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol),
                },
                /*
                 ALTER TABLE table_name ALTER COLUMN column_name SET NOT NULL;
                 ALTER TABLE table_name ALTER COLUMN column_name DROP NOT NULL;

                 ALTER TABLE table_name ADD COLUMN column_name datatype; 
                 ALTER TABLE table_name DROP COLUMN column_name; 

                 ALTER TABLE table_name ADD CONSTRAINT UC_Person UNIQUE (ID,LastName); 
                 ALTER TABLE table_name DROP CONSTRAINT UC_Person; 
                 */
                new List<ParserBase>
                {
                    new ParserBase("Alter column", QueryPartType.AlterColumn,
                    new List<RequiredToken>
                    {
                        new RequiredToken("ALTER"),
                        new RequiredToken("COLUMN"),
                        new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
                    },
                    new List<ParserBase>
                    {
                        new ParserBase("Set column not nullable", QueryPartType.SetNotNullable,
                        new List<RequiredToken>
                        {
                            new RequiredToken("SET"),
                            new RequiredToken("NOT"),
                            new RequiredToken("NULL")
                        }),
                        new ParserBase("Drop column not nullable", QueryPartType.DropNotNullable,
                        new List<RequiredToken>
                        {
                            new RequiredToken("DROP"),
                            new RequiredToken("NOT"),
                            new RequiredToken("NULL")
                        })
                    }).AcceptOnlyOneParserPerGroup(),
                    new ParserBase("Add column or constraint", QueryPartType.AddColumnOrConstraint,
                        new List<RequiredToken>
                        {
                            new RequiredToken("ADD")
                        },
                        new List<ParserBase>
                        {
                            new ParserBase("Add column", QueryPartType.ColumnDefinition,
                            new List<RequiredToken>
                            {
                                new RequiredToken("COLUMN"),
                                new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol),
                                new RequiredToken(QueryPropertyValue.DataType, TokenType.Keyword, new List<string> { "INTEGER", "STRING" }),
                                notNull
                            }),
                            defineRule
                        }
                    ).AcceptOnlyOneParserPerGroup(),
                    new ParserBase("Drop column or constraint", QueryPartType.DropColumnOrRule,
                        new List<RequiredToken>
                        {
                            new RequiredToken("DROP")
                        },
                        new List<ParserBase>
                        {
                            new ParserBase("Drop column", QueryPartType.DropColumn,
                                new List<RequiredToken>
                                {
                                    new RequiredToken("COLUMN"),
                                    new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol)
                                }),
                            new ParserBase("Drop constraint", QueryPartType.DropRule,
                                new List<RequiredToken>
                                {
                                    new RequiredToken("CONSTRAINT"),
                                    new RequiredToken(QueryPropertyValue.RuleName, TokenType.Symbol)
                                }
                            )
                        }
                    ).AcceptOnlyOneParserPerGroup()
                });

            var selectParser = new ParserBase("Select statement", QueryPartType.Select,
                new List<RequiredToken>
                {
                    new RequiredToken("SELECT")
                },
                new List<List<ParserBase>> {
                new List<ParserBase>
                {
                    new ParserBase("Selected columns definition", QueryPartType.From,
                        new List<RequiredToken>(),
                        new List<ParserBase>
                        {
                            new ParserBase("Select single column definition", QueryPartType.ColumnName,
                            new List<RequiredToken>
                            {
                                new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol),
                                new RequiredToken("AS", true, new RequiredToken(QueryPropertyValue.Alias, TokenType.Symbol))
                            })
                        },
                        new List<RequiredToken> {
                            new RequiredToken("FROM"),
                            new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol),
                            new RequiredToken("AS", true, new RequiredToken(QueryPropertyValue.Alias, TokenType.Symbol))
                        }
                    ),
                    new ParserBase("Select *", QueryPartType.From,
                        new List<RequiredToken>
                        {
                            new RequiredToken("*", false, null, QueryPropertyValue.ColumnName),
                            new RequiredToken("FROM"),
                            new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol),
                            new RequiredToken("AS", true, new RequiredToken(QueryPropertyValue.Alias, TokenType.Symbol))
                        }
                    )
                },
                new List<ParserBase>
                {
                    whereParser
                },
            });

            var insertParser = new ParserBase("Insert statement", QueryPartType.Insert, new List<RequiredToken>
            {
                new RequiredToken("INSERT"),
                new RequiredToken("INTO"),
                new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol)
            },
            new List<List<ParserBase>>
            {
                new List<ParserBase> {
                    new ParserBase("Inserted columns", QueryPartType.InsertedColumns, new List<RequiredToken>
                    {
                        new RequiredToken("(")
                    },
                    new List<ParserBase>
                    {
                        columnParser
                    },
                    new List<RequiredToken>
                    {
                        new RequiredToken(")")
                    })
                },
                new List<ParserBase>
                {
                    new ParserBase("Inserted values", QueryPartType.InsertedValues, new List<RequiredToken>
                    {
                        new RequiredToken("VALUES"),
                        new RequiredToken("(")
                    },
                    new List<ParserBase>
                    {
                        stringParser,
                        numberParser,
                        nullParser
                    },
                    new List<RequiredToken> {
                        new RequiredToken(")")
                    })
                }
            }).WithMandatoryParsers(new int[] { 1 });

            var deleteParser = new ParserBase("Delete statement", QueryPartType.Delete,
            new List<RequiredToken>
            {
                new RequiredToken("DELETE"),
                new RequiredToken("FROM"),
                new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol)
            },
            new List<ParserBase>
            {
                whereParser
            }).WithMandatoryParsers(new int[] { });

            var beginTransaction = new ParserBase("Begin transaction", QueryPartType.BeginTransaction, new List<RequiredToken>
            {
                new RequiredToken("BEGIN"),
                new RequiredToken("TRANSACTION")
            });

            var commitTransaction = new ParserBase("Commit transaction", QueryPartType.CommitTransaction, new List<RequiredToken> { new RequiredToken("COMMIT") });
            var rollbackTransaction = new ParserBase("Rollback transaction", QueryPartType.RollbackTransaction, new List<RequiredToken> { new RequiredToken("ROLLBACK") });

            var updateParser = new ParserBase("Update rows", QueryPartType.Update, new List<RequiredToken>
            {
                new RequiredToken("UPDATE"),
                new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol),
                new RequiredToken("SET"),
            },
            new List<List<ParserBase>>
            {
                new List<ParserBase>
                {
                    new ParserBase("Update column definition", QueryPartType.UpdateColumnDefinition, new List<RequiredToken>
                    {
                        new RequiredToken(QueryPropertyValue.ColumnName, TokenType.Symbol),
                        new RequiredToken("=")
                    },
                    new List<ParserBase>
                    {
                        numberParser,
                        stringParser,
                        nullParser
                    }).AcceptOnlyOneParserPerGroup()
                },
                new List<ParserBase>
                {
                    whereParser
                }
            }
            );

            var dropTableParser = new ParserBase("Drop table", QueryPartType.DropTable,
                new List<RequiredToken>
                {
                    new RequiredToken("DROP"),
                    new RequiredToken("TABLE"),
                    new RequiredToken(QueryPropertyValue.TableName, TokenType.Symbol)
                }
            );

            return new ParserBase("Query parser", QueryPartType.BaseQuery, new List<RequiredToken>(),
                new List<ParserBase>
                {
                    tableParser,
                    dropTableParser,
                    selectParser,
                    insertParser,
                    deleteParser,
                    updateParser,
                    alterTableParser,
                    beginTransaction,
                    commitTransaction,
                    rollbackTransaction
                },
                new List<RequiredToken>
                {
                    new RequiredToken("", false, null, null, TokenType.EndOfString)
                }
            );
        }

        public static QueryPart ParseQuery(string queryString)
        {
            var scanner = new Scanner(queryString);
            return ParserBase.ParseToken(scanner).Subparts[0];   
        }
    }
}
