﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Enums
{
    public enum QueryPartType
    {
        BaseQuery,
        CreateTable,
        Insert,
        InsertedColumns,
        InsertedValues,
        ColumnDefinition,
        RuleDefinition,
        ColumnList,
        Delete,
        Select,
        From,
        Where,
        Condition,
        Integer,
        ColumnName,
        String,
        Delimiter,
        Null,
        ConditionBracket,
        Constraint,
        AlterTable,
        AlterColumn,
        SetNotNullable,
        DropNotNullable,
        DropColumn,
        AddColumnOrConstraint,
        DropColumnOrRule,
        DropRule,
        BeginTransaction,
        CommitTransaction,
        RollbackTransaction,
        Update,
        UpdateColumnDefinition,
        DropTable
    }
}
