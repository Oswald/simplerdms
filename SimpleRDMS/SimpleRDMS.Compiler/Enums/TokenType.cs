﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Enums
{
    public enum TokenType
    {
        /// <summary>
        /// CREATE, UPDATE, etc
        /// </summary>
        Keyword,
        /// <summary>
        /// * ( ) ,
        /// </summary>
        SpecialCharacter,
        /// <summary>
        /// Any text between ""
        /// </summary>
        String,
        Integer,
        /// <summary>
        /// Words, that are not keywords
        /// </summary>
        Symbol,
        EndOfString
    }
}
