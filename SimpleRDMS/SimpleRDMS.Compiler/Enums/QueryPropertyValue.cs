﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Compiler.Enums
{
    public enum QueryPropertyValue
    {
        TableName,
        ColumnName,
        RuleType,
        DataType,
        NotNullable,
        Alias,
        Operator,
        LogicalConnectiveType,
        Integer,
        String,
        Null,
        RuleName
    }
}
