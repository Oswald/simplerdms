﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Entities
{
    public class RowData
    {
        public string TableName { get; set; }
        public Guid Id { get; set; }
        public object[] Row { get; set; }
    }
}
