﻿
using SimpleRDMS.Recovery.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Entities
{
    public class LogEntry
    {
        public LogEntryType Type { get; set; }
        public RowData Data { get; set; }
    }
}
