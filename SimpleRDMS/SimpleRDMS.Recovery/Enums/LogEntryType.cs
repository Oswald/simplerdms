﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Enums
{
    public enum LogEntryType
    {
        BeginTransaction,
        CommitTransaction,
        RollbackTransaction,
        DataChanged
    }
}
