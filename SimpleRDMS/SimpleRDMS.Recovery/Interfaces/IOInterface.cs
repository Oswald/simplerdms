﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Interfaces
{
    public interface IOInterface
    {
        void WriteLines(IEnumerable<string> lines);
        IEnumerable<string> ReadLines();
        void WriteBytes(byte[] content);
        byte[] ReadBytes();
        void Clear();
    }
}
