﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace SimpleRDMS.Recovery.Interfaces
{
    public class DiskInterface : IOInterface
    {
        string FileName { get; }
        string NewFileName { get; }
        string BackupFileName { get; set; }
        public DiskInterface(string fileName, string extenstion)
        {
            FileName = $"{fileName}.{extenstion}";
            NewFileName = $"{fileName}_new.{extenstion}";
            BackupFileName = $"{fileName}_backup.{extenstion}";

            if (File.Exists(NewFileName))
            {
                ReplaceFiles();
            }
            else if (!File.Exists(FileName))
            {
                File.Create(FileName).Close();
            }
        }

        public void WriteLines(IEnumerable<string> lines)
        {
            try
            {
                File.AppendAllLines(FileName, lines);
            }
            catch (Exception e)
            {
                throw new Exception($"Could not write lines to {FileName}", e);
            }

        }

        public IEnumerable<string> ReadLines()
        {
            try
            {
                return File.ReadAllLines(FileName);
            }
            catch (Exception e)
            {
                throw new Exception($"Could not read lines from {FileName}", e);
            }
        }

        public void WriteBytes(byte[] content)
        {
            try
            {
                using (var fs = new FileStream(NewFileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(content, 0, content.Length);
                }
                ReplaceFiles();
            }
            catch (Exception e)
            {
                throw new Exception($"Could not write bytes to {NewFileName}", e);
            }

        }

        public byte[] ReadBytes()
        {
            try
            {
                return File.ReadAllBytes(FileName);
            }
            catch (Exception e)
            {
                throw new Exception($"Could not read lines from {FileName}", e);
            }
        }

        void ReplaceFiles()
        {
            try
            {
                if (File.Exists(NewFileName))
                {
                    if (!File.Exists(FileName))
                    {
                        File.Create(FileName).Close();
                    }
                    if (File.Exists(BackupFileName))
                    {
                        File.Delete(BackupFileName);
                    }

                    File.Replace(NewFileName, FileName, BackupFileName);
                }
                else
                {
                    throw new Exception($"File {NewFileName} does not exist");
                }

            }
            catch (Exception e)
            {
                throw new Exception($"Could not replace {FileName} with {NewFileName}", e);
            }
        }

        public void Clear()
        {
            try 
            { 
                File.WriteAllText(FileName, "");
            }
            catch (Exception e)
            {
                throw new Exception($"Could clear file {FileName}", e);
            }
        }
    }
}
