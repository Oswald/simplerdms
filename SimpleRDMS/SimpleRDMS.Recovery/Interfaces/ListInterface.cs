﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Interfaces
{
    public class ListInterface : IOInterface
    {
        public List<string> Lines { get; } = new List<string>();
        public byte[] Bytes { get; set; } = null;

        public void Clear()
        {
            Lines.Clear();
        }

        public byte[] ReadBytes()
        {
            return Bytes;
        }

        public IEnumerable<string> ReadLines()
        {
            return Lines;
        }

        public void WriteBytes(byte[] content)
        {
            Bytes = content;
        }

        public void WriteLines(IEnumerable<string> lines)
        {
            Lines.AddRange(lines);
        }
    }
}
