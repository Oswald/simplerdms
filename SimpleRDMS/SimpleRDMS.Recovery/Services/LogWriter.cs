﻿using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Recovery.Entities;
using SimpleRDMS.Recovery.Enums;
using SimpleRDMS.Recovery.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimpleRDMS.Recovery.Services
{
    public class LogWriter
    {
        IOInterface LogInterface { get; }
        public LogWriter(IOInterface logInterface)
        {
            LogInterface = logInterface;
        }

        public void WriteChangelog(IEnumerable<ResultBase> results)
        {
            var entries = new List<LogEntry>();

            foreach(var result in results)
            {
                if (result.HasLogEntry)
                {
                    var entry = (ResultWithLogEntry)result;
                    entries.Add(new LogEntry
                    {
                        Type = LogEntryType.DataChanged,
                        Data = new RowData
                        {
                            Row = entry.Data,
                            Id = entry.RowId,
                            TableName = entry.TableName
                        }
                    });
                }
            }

            Write(entries);
        }

        void Write(List<LogEntry> entries)
        {
            string[] lines = new string[entries.Count];

            for(int i = 0; i < entries.Count; i++)
            {
                lines[i] = System.Text.Json.JsonSerializer.Serialize(entries[i]);
            }

            LogInterface.WriteLines(lines);
        }
    }
}
