﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Recovery.Services
{
    public static class DatabaseInitializer
    {
        public static Database InitializeDatabase()
        {
            var tables = new List<Table>();

            var masterTables = new Table
            {
                Name = "master_tables",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "name",
                        Nullable = false,
                        Type = DataType.String
                    }
                }
            };

            masterTables.CreateUniqueConstraint("table_name_is_unique", new string[] { "name" });


            var masterColumns = new Table
            {
                Name = "master_columns",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "table_name",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "column_name",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "type",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "nullable",
                        Nullable = false,
                        Type = DataType.Integer
                    }
                }
            };

            masterColumns.CreateUniqueConstraint("column_name_is_unique_within_table", new string[] { "column_name", "table_name" });

            var masterRules = new Table
            {
                Name = "master_rules",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "table_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "rule_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "rule_type",
                        Type = DataType.String,
                        Nullable = false
                    }
                }
            };

            masterRules.CreateUniqueConstraint("rule_name_is_unique", new string[] { "rule_name" });

            var masterRuleColumns = new Table
            {
                Name = "master_rule_columns",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "rule_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "column_name",
                        Type = DataType.String,
                        Nullable = false
                    }
                }
            };

            masterRuleColumns.CreateUniqueConstraint("colum_can_belong_to_rule_only_once", new string[] { "rule_name", "column_name" });

            tables.Add(masterTables);
            tables.Add(masterColumns);
            tables.Add(masterRules);
            tables.Add(masterRuleColumns);

            return new Database(tables);
        }
    }
}
