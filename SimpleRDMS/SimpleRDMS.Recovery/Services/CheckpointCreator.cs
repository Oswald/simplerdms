﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Recovery.Entities;
using SimpleRDMS.Recovery.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace SimpleRDMS.Recovery.Services
{
    public class CheckpointCreator
    {
        IOInterface LogProvider { get; }
        IOInterface Checkpoint { get; }
        Mutex DiskAccessMutex { get; }
        public CheckpointCreator(IOInterface logProvider, IOInterface checkpoint, Mutex diskAccessMutex)
        {
            LogProvider = logProvider;
            Checkpoint = checkpoint;
            DiskAccessMutex = diskAccessMutex;
        }

        /// <summary>
        /// Creates checkpoint from previous checkpoint and logs, and returns current database. Clears logs and previous checkpoint. Writing queries are not accepted when creating a new checkpoint.
        /// </summary>
        public Database CreateCheckpoint()
        {
            try
            {
                DiskAccessMutex.WaitOne();

                var checkpoint = Checkpoint.ReadBytes();
                var database = checkpoint == null || checkpoint.Length == 0 ? DatabaseInitializer.InitializeDatabase() : DeserializeDatabase(checkpoint);
                var logs = LogProvider.ReadLines().Select(log => DeserializeLogEntry(log)).ToList();

                AddLogEntriesToDatabase(database, logs);
                Checkpoint.WriteBytes(SerializeDatabase(database));
                LogProvider.Clear();

                return database;
            }
            catch (Exception e)
            {
                throw new Exception("Error when creating a new checkpoint", e);
            }
            finally
            {
                DiskAccessMutex.ReleaseMutex();
            }
        }

        LogEntry DeserializeLogEntry(string logText)
        {
            try
            {
                var entry = JsonSerializer.Deserialize<LogEntry>(logText);
                if (entry.Data.Row != null)
                {
                    var row = new object[entry.Data.Row.Length];

                    for (int i = 0; i < entry.Data.Row.Length; i++)
                    {
                        var cell = entry.Data.Row[i];
                        if (cell == null)
                        {
                            row[i] = null;
                        }
                        else
                        {
                            var value = (JsonElement)entry.Data.Row[i];
                            if (value.ValueKind == JsonValueKind.String)
                            {
                                row[i] = value.GetString();
                            }
                            else if (value.ValueKind == JsonValueKind.Number)
                            {
                                row[i] = value.GetInt32();
                            }
                            else
                            {
                                throw new Exception($"Unknown value type: {value.ValueKind}");
                            }
                        }
                    }

                    entry.Data.Row = row;
                }
                return entry;
            }
            catch (Exception e)
            {
                throw new Exception($"Could not serialize log entry: {logText}", e);
            }
        }

        void AddLogEntriesToDatabase(Database database, List<LogEntry> entries)
        {
            //TODO: also add data to master tables on default

            Tuple<Table, string> rule = null; //If new rule is created, table and rule name are saved here
            List<string> ruleColumns = null; //Rule columns are saved here, before creating the rule'

            void HandleUniqueConstraint()
            {
                if(!rule.Item1.UniqueConstraints.Any(u => u.Name == rule.Item2))
                {
                    rule.Item1.CreateUniqueConstraint(rule.Item2, ruleColumns.ToArray());
                    rule = null;
                    ruleColumns = null;
                }
            }

            foreach (var entry in entries) 
            {
                try
                {
                    switch (entry.Data.TableName) //Handle master tables
                    {
                        case "master_tables":
                            HandleTable(database, entry);
                            break;
                        case "master_columns":
                            HandleColumn(database, entry);
                            break;
                        case "master_rules":
                            if (ruleColumns != null) //Two rules sequentially; handle first before starting second
                            {
                                HandleUniqueConstraint();
                            }
                            rule = HandleRule(database, entry);
                            if (rule != null) //Only create, if rule was added
                            {
                                ruleColumns = new List<string>();
                            }
                            break;
                        case "master_rule_columns":
                            if (entry.Data.Row != null)
                            {
                                ruleColumns.Add((string)entry.Data.Row[1]);
                            }
                            break;
                        default:
                            if (rule != null)
                            {
                                HandleUniqueConstraint();
                            }
                            break;
                    }

                    //Now, update log to database
                    var table = database.GetTable(entry.Data.TableName);
                    var existingValue = table.FirstOrDefault(v => v.Id == entry.Data.Id);

                    if (entry.Data.Row == null) //Row was deleted
                    {
                        if (existingValue != null)
                        {
                            table.Remove(existingValue);
                        }
                    }
                    else //Row added or updated
                    {
                        if (existingValue == null) //Added
                        {
                            table.Insert(entry.Data.Row, entry.Data.Id);
                        }
                        else
                        {
                            table.Update(existingValue, entry.Data.Row);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Could not handle log entry", e);
                }
            }

            if (rule != null) //If last log entry was rule column, switch.default is never entered. Therefore, this must be here.
            {
                HandleUniqueConstraint();
            }
        }
        /// <summary>
        /// Returns table and rule name
        /// </summary>
        /// <param name="database"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        Tuple<Table, string> HandleRule(Database database, LogEntry entry)
        {
            if (entry.Data.Row == null) //Row was deleted -> fetch previous value and drop rule
            {
                var previousValue = GetPreviousValue(database, "master_rules", entry.Data.Id);
                if (previousValue != null) //Log entry was handled previously
                {
                    var table = (string)previousValue["table_name"];
                    var rule = (string)previousValue["rule_name"];
                    database.GetTable(table).RemoveUniqueConstraint(rule);
                }
            }
            else
            {
                var tableName = (string)entry.Data.Row[0];
                var ruleName = (string)entry.Data.Row[1];
                var table = database.GetTable(tableName);

                if(!table.UniqueConstraints.Any(u => u.Name == ruleName))
                {
                    return new Tuple<Table, string>(table, ruleName);
                }
            }

            return null;
        }

        void HandleTable(Database database, LogEntry entry)
        {
            if (entry.Data.Row == null) //Row was deleted -> fetch previous value and drop table
            {
                var previousValue = GetPreviousValue(database, "master_tables", entry.Data.Id);
                if(previousValue != null) //Log entry was handled previously
                {
                    database.DropTable((string)previousValue["name"]);
                }
            }
            else //Row was added -> create new table
            {
                var tableName = (string)entry.Data.Row[0];
                if (!database.TableExists(tableName))
                {
                    database.AddTable(new Table { Name = tableName });
                }
            }
        }

        void HandleColumn(Database database, LogEntry entry)
        {
            if (entry.Data.Row == null) //Column was deleted
            {
                var previousValue = GetPreviousValue(database, "master_columns", entry.Data.Id);
                if (previousValue != null) //Log entry was handled previously
                {
                    var tableName = (string)previousValue["table_name"];
                    database.GetTable(tableName).RemoveColumn((string)previousValue["column_name"]);
                }
            }
            else //Column was added or changed
            {
                var tableName = (string)entry.Data.Row[0];
                var columnName = (string)entry.Data.Row[1];
                var type = (DataType)Enum.Parse(typeof(DataType), (string)entry.Data.Row[2]);
                var nullable = (int)entry.Data.Row[3] == 1;

                var table = database.GetTable(tableName);
                var column = table.Columns.FirstOrDefault(c => c.Name == columnName);
                if (column == null) //Was added
                {
                    table.CreateColumn(new Column { Name = columnName, Nullable = nullable, Type = type });
                }
                else if(nullable) //Null constraint removed
                {
                    if (!column.Nullable)
                    {
                        table.RemoveNotNullConstraint(columnName);
                    }
                }
                else //null constraint added
                {
                    if (column.Nullable)
                    {
                        table.CreateNotNullConstraint(columnName);
                    }
                }
            }

                
        }

        DatabaseRow GetPreviousValue(Database database, string tableName, Guid rowId)
        {
            var table = database.GetTable(tableName);
            foreach(var row in table)
            {
                if(row.Id == rowId)
                {
                    return row;
                }
            }

            return null;
        }

        Database DeserializeDatabase(byte[] arrBytes)
        {

            BinaryFormatter formatter = new BinaryFormatter();

            using (MemoryStream memStream = new MemoryStream(arrBytes))
            {
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                Database message = (Database)formatter.Deserialize(memStream);
                return message;
            }
        }

        byte[] SerializeDatabase(Database database)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, database);

                return ms.ToArray();
            }
        }

    }
}
