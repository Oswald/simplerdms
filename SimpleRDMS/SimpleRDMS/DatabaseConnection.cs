﻿using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Domain.Exceptions;
using SimpleRDMS.Recovery.Interfaces;
using SimpleRDMS.Recovery.Services;
using System;
using System.IO;
using System.Threading;

namespace SimpleRDMS
{
    public class DatabaseConnection
    {
        QueryRunner QueryRunner { get; }

        public DatabaseConnection(string path)
        {
            var logFile = new DiskInterface(Path.Combine(path, "log"), "txt");
            var checkpointFile = new DiskInterface(Path.Combine(path, "checkpoint"), "txt");

            var mutex = new Mutex();

            var checkpoint = new CheckpointCreator(logFile, checkpointFile, mutex);

            QueryRunner = new QueryRunner(checkpoint.CreateCheckpoint(), new LogWriter(logFile), mutex); 
        }

        public QueryResult ExecuteQuery(string query)
        {
            try
            {
                return new QueryResult
                {
                    Success = true,
                    Exception = null,
                    Results = QueryRunner.ExecuteQuery(ParseQuery(query))
                };
            }
            catch (SimpleRDMSUserException e)
            {
                return new QueryResult
                {
                    Success = false,
                    Exception = GetFirstException(e),
                    Results = null
                };
            }
        }

        public QueryPart ParseQuery(string query)
        {
            try
            {
                return QueryParser.ParseQuery(query.Trim(';'));
            }
            catch (SimpleRDMSException e)
            {
                throw new SimpleRDMSUserException($"Could not parse query. {e.Message}");
            }
        }

        public Exception GetFirstException(Exception e)
        {
            if(e.InnerException != null)
            {
                return GetFirstException(e.InnerException);
            }
            else
            {
                return e;
            }
        }
    }
}
