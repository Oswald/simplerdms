﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS
{
    public class QueryResult
    {
        public bool Success { get; set; }
        public IEnumerable<Row> Results { get; set; }
        public Exception Exception { get; set; }
    }
}
