﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Domain.Exceptions;
using SimpleRDMS.Recovery.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SimpleRDMS.Core.QueryHandlers
{
    public class QueryRunner
    {
        Database Database { get; }
        LogWriter LogWriter { get; }
        List<ResultBase> Transaction { get; set; } = null; // If null, no transaction is active
        Mutex DiskAccessMutex { get; }
        public QueryRunner(Database database, LogWriter logWriter, Mutex diskAccessMutex)
        {
            Database = database;
            LogWriter = logWriter;
            DiskAccessMutex = diskAccessMutex;
        }

        public IEnumerable<Row> ExecuteQuery(QueryPart query)
        {
            var queryResults = new Stack<ResultBase>();
            IEnumerable<Row> data = null;

            void RevertQuery()
            {
                try
                {
                    while (queryResults.Count > 0)
                    {
                        queryResults.Pop().Revert();
                    }
                } catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("Reverting query effects failed. The datatabase is not likely to recover from this without a restart.", e);
                }
            }

            try
            {
                switch (query.Type)
                {
                    case QueryPartType.BeginTransaction:
                        BeginTransaction();
                        break;
                    case QueryPartType.CommitTransaction:
                        CommitTransaction();
                        break;
                    case QueryPartType.RollbackTransaction:
                        RollbackTransaction();
                        break;
                    case QueryPartType.AlterTable:
                        AlterTableRunner.ExecuteQuery(query, Database, queryResults, false);
                        break;
                    case QueryPartType.Select:
                        data = SelectQueryRunner.ExecuteQuery(query, Database).ToList();
                        break;
                    case QueryPartType.Update:
                        UpdateQueryRunner.ExecuteQuery(query, Database, queryResults, false);
                        break;
                    case QueryPartType.Insert:
                        InsertQueryRunner.ExecuteQuery(query, Database, queryResults, false);
                        break;
                    case QueryPartType.Delete:
                        DeleteQueryRunner.ExecuteQuery(query, Database, queryResults, false);
                        break;
                    case QueryPartType.CreateTable:
                        CreateTableRunner.ExecuteQuery(query, Database, queryResults);
                        break;
                    case QueryPartType.DropTable:
                        DropTableRunner.ExecuteQuery(query, Database, queryResults, false);
                        break;
                    default:
                        throw new SimpleRDMSInternalException($"Unknown query type: {query.Type}");
                }
            }
            catch (SimpleRDMSException e)
            {
                RevertQuery();
                throw new SimpleRDMSUserException($"Excecuting query of type {query.Type} failed.", e);
            }
            catch (Exception e)
            {
                RevertQuery();
                throw new SimpleRDMSInternalException($"Excecuting query of type {query.Type} failed.", e);
            }

            var reversedResults = queryResults.Reverse();

            if (Transaction != null)
            {
                Transaction.AddRange(reversedResults);
            }
            else
            {
                try
                {
                    DiskAccessMutex.WaitOne();
                    LogWriter.WriteChangelog(reversedResults);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("Could not write logs to disk", e);
                }
                finally
                {
                    DiskAccessMutex.ReleaseMutex();
                }
            }

            return data;
        }

        void BeginTransaction()
        {
            if(Transaction == null)
            {
                Transaction = new List<ResultBase>();
            }
            else
            {
                throw new SimpleRDMSUserException("Cannot begin transaction. There is already an open transaction.");
            }
        }

        void CommitTransaction()
        {
            if(Transaction == null)
            {
                throw new SimpleRDMSUserException("There is no open transaction to be commited.");
            }
            else
            {
                LogWriter.WriteChangelog(Transaction);

                Transaction = null;
            }
        }

        void RollbackTransaction()
        {
            if (Transaction == null)
            {
                throw new SimpleRDMSUserException("There is no open transaction to be reverted.");
            }
            else
            {
                for(int i = Transaction.Count - 1; i >= 0; i --)
                {
                    Transaction[i].Revert();
                }
                Transaction = null;
            }
        }
    }
}
