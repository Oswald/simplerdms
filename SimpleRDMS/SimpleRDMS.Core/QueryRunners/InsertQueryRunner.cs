﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QuerySets;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class InsertQueryRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results, bool allowMaster)
        {
            //TODO: Currently only one insert can be made per query, although there is support for many.
            //TODO: Also this should maybe return something.

            var tableName = query.Properties[Compiler.Enums.QueryPropertyValue.TableName];

            if (!allowMaster && (tableName == "master_tables" || tableName == "master_columns" || tableName == "master_rules" || tableName == "master_rule_columns"))
            {
                throw new SimpleRDMSException($"Only SELECT-queries are allowed to {tableName}");
            }

            var handler = new InsertHandler(
                new Entities.InsertProperties(database.GetTable(tableName), 
                GetInsertedValuesOnCorrectIndices(query, database)));

            handler.InsertRows(results);
            return null;
        }

        static List<object[]> GetInsertedValuesOnCorrectIndices(QueryPart query, Database database)
        {
            var insertedColumns = query.Subparts.SingleOrDefault(q => q.Type == Compiler.Enums.QueryPartType.InsertedColumns);
            var insertedValues = GetInsertedValues(query);
            if(insertedColumns == null)
            {
                return new List<object[]> { insertedValues };
            }
            else
            {
                var columnNames = insertedColumns.Subparts.Select(s => s.Properties[Compiler.Enums.QueryPropertyValue.ColumnName]).ToArray();

                var targetTable = database.GetTable(query.Properties[Compiler.Enums.QueryPropertyValue.TableName]);
                var valuesArray = new object[targetTable.Columns.Length];

                if(columnNames.Length != insertedValues.Length)
                {
                    throw new Exception("If table names are specified, there must be same amount of values in the insert");
                }
                else if(columnNames.Distinct().Count() != columnNames.Length)
                {
                    throw new Exception("Table name was specified multiple times on insert");
                }

                for(int i = 0; i < targetTable.Columns.Length; i++)
                {
                    int indexOfColumnName = -1;

                    for(int j = 0; j < columnNames.Length; j++)
                    {
                        if(targetTable.Columns[i].Name == columnNames[j])
                        {
                            indexOfColumnName = j;
                        };
                    }

                    if(indexOfColumnName == -1) //This column has no value on insert
                    {
                        valuesArray[i] = null;
                    }
                    else 
                    {
                        valuesArray[i] = insertedValues[indexOfColumnName];
                    }
                }

                return new List<object[]> { valuesArray };
            }
        }

        static object[] GetInsertedValues(QueryPart query)
        {
            return query.Subparts.Where(q => q.Type == Compiler.Enums.QueryPartType.InsertedValues)
                .SelectMany(q => q.Subparts)
                .Select(s => ExecuteQueryQueryPartToValue(s))
                .ToArray();
        }

        public static object ExecuteQueryQueryPartToValue(QueryPart part)
        {
            if(part.Type == Compiler.Enums.QueryPartType.Integer)
            {
                return Int32.Parse(part.Properties[Compiler.Enums.QueryPropertyValue.Integer]);
            }
            else if (part.Type == Compiler.Enums.QueryPartType.String)
            {
                return part.Properties[Compiler.Enums.QueryPropertyValue.String];
            }
            else if (part.Type == Compiler.Enums.QueryPartType.Null)
            {
                return null;
            }
            else
            {
                throw new Exception("QueryPart has unknown value");
            }
        }
    }
}
