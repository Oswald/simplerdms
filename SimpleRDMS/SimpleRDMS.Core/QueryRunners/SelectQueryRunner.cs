﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class SelectQueryRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database)
        {
            var selector = new SelectHandler(query.Subparts
                .Where(q => q.Type == Compiler.Enums.QueryPartType.From)
                .Select(q => ParseFrom(q, database))
                .ToList());

            var where = query.Subparts.SingleOrDefault(q => q.Type == QueryPartType.Where);
            if (where == null)
            {
                return selector;
            }
            else
            {
                return WhereQueryRunner.ExecuteQuery(where, database, selector);
            }
        }

        static SelectProperties ParseFrom(QueryPart query, Database database)
        {

            return new SelectProperties(
                GetAlias(query),
                database.GetTable(query.Properties[QueryPropertyValue.TableName]),
                GetColumnProperties(query, database));
        }

        static string GetAlias(QueryPart query)
        {
            return query.Properties.ContainsKey(QueryPropertyValue.Alias) ? query.Properties[QueryPropertyValue.Alias] : query.Properties[QueryPropertyValue.TableName];
        }

        static ColumnProperties[] GetColumnProperties(QueryPart query, Database database)
        {

            if (query.Properties.ContainsKey(QueryPropertyValue.ColumnName) && query.Properties[QueryPropertyValue.ColumnName] == "*")
            {
                return database.GetTable(query.Properties[QueryPropertyValue.TableName])
                    .Columns
                    .Select(c => new ColumnProperties
                    {
                        ColumnName = c.Name,
                        ColumnAlias = c.Name
                    }).ToArray();
            }
            else
            {
                return query.Subparts.Where(s => s.Type == QueryPartType.ColumnName)
                    .Where(s => s.Properties[QueryPropertyValue.ColumnName] != "*")
                    .Select(c => new ColumnProperties
                    {
                        ColumnAlias = c.Properties.ContainsKey(QueryPropertyValue.Alias) ? c.Properties[QueryPropertyValue.Alias] : c.Properties[QueryPropertyValue.ColumnName],
                        ColumnName = c.Properties[QueryPropertyValue.ColumnName]
                    })
                    .ToArray();
            }
        }


    }
}
