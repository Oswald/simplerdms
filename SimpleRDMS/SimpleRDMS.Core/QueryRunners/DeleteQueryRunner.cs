﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QuerySets;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class DeleteQueryRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results, bool allowMaster)
        {
            var tableName = query.Properties[Compiler.Enums.QueryPropertyValue.TableName];

            if (!allowMaster && (tableName == "master_tables" || tableName == "master_columns" || tableName == "master_rules" || tableName == "master_rule_columns"))
            {
                throw new SimpleRDMSException($"Only SELECT-queries are allowed to {tableName}");
            }

            var where = query.Subparts.SingleOrDefault(s => s.Type == Compiler.Enums.QueryPartType.Where);
            var handler = new DeleteHandler(new Entities.DeleteProperties(database.GetTable(tableName), where == null ? null : WhereQueryRunner.GetCondition(where.Subparts, database)));
            handler.RemoveRows(results);
            return null;
        }
    }
}
