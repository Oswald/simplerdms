﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class CreateTableRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results)
        {
            var tableName = query.Properties[QueryPropertyValue.TableName];

            if (tableName.Contains('.'))
            {
                throw new SimpleRDMSException("Table name cannot contain '.'");
            }

            var columns = query.Subparts.Where(s => s.Type == QueryPartType.ColumnDefinition).Select(c => ParseColumn(c)).ToArray();
            var ruleQueries = query.Subparts.Where(s => s.Type == QueryPartType.RuleDefinition);

            try
            {
                var table = new Table
                {
                    Columns = columns,
                    Name = tableName
                };

                
                results.Push(database.AddTable(table));
                AddTableToMaster(tableName, columns, database, results);

                foreach(var rule in ruleQueries)
                {
                    AddRulesToMaster(rule, database, table, results);
                }

                foreach (var rule in ruleQueries)
                {
                    results.Push(table.CreateUniqueConstraint(rule.Properties[QueryPropertyValue.RuleName], rule.Subparts.Where(q => q.Type == QueryPartType.ColumnName).Select(r => r.Properties[QueryPropertyValue.ColumnName]).ToArray()));
                }

            } catch (Exception e)
            {
                throw new SimpleRDMSException("Could not create table", e);
            }

            return null;
        }

        public static Column ParseColumn(QueryPart column)
        {
            if (column.Properties[QueryPropertyValue.ColumnName].Contains('.'))
            {
                throw new SimpleRDMSException("Column name cannot contain '.'");
            }

            return new Column
            {
                Name = column.Properties[QueryPropertyValue.ColumnName],
                Type = (Buffer.Enums.DataType)Enum.Parse(typeof(Buffer.Enums.DataType), column.Properties[QueryPropertyValue.DataType], true),
                Nullable = !column.Properties.ContainsKey(QueryPropertyValue.NotNullable)
            };
        }

        static void AddTableToMaster(string tableName, Column[] columns, Database database, Stack<ResultBase> results)
        {
            try
            {
                InsertQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"INSERT INTO master_tables (name) VALUES(\"{tableName}\")"), database, results, true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("INSERT-operation to master_tables threw an exception.", e);
            }

            foreach (var column in columns)
            {
                var nullable = column.Nullable ? 1 : 0;
                try
                {
                    var query = QueryParser.ParseQuery($"INSERT INTO master_columns (table_name, column_name, type, nullable) VALUES (\"{tableName}\", \"{column.Name}\", \"{column.Type}\", {nullable})");
                    InsertQueryRunner.ExecuteQuery(query, database, results, true);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("INSERT-operation to master_columns threw an exception.", e);
                }
            }
        }

        public static void AddRulesToMaster(QueryPart query, Database database, Table table, Stack<ResultBase> results)
        {
            //TODO: All rules are unique constraints, for now
            try
            {
                InsertQueryRunner.ExecuteQuery(
                    QueryParser.ParseQuery($"INSERT INTO master_rules (table_name, rule_name, rule_type) VALUES (\"{table.Name}\", \"{query.Properties[QueryPropertyValue.RuleName]}\", \"Unique\")"),
                    database,
                    results, 
                    true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("INSERT-operation to master_rules threw an exception.", e);
            }

            var columns = query.Subparts.Where(q => q.Type == QueryPartType.ColumnName);

            foreach(var column in columns)
            {
                try
                {
                    InsertQueryRunner.ExecuteQuery(
                        QueryParser.ParseQuery($"INSERT INTO master_rule_columns (rule_name, column_name) VALUES (\"{query.Properties[QueryPropertyValue.RuleName]}\", \"{column.Properties[QueryPropertyValue.ColumnName]}\")"),
                        database,
                        results,
                        true
                    );
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("INSERT-operation to master_rule_columns threw an exception.", e);
                }
            }
        }
    }
}
