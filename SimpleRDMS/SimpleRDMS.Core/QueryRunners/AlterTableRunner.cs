﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class AlterTableRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results, bool allowMaster)
        {

            var tableName = query.Properties[Compiler.Enums.QueryPropertyValue.TableName];

            if (!allowMaster && (tableName == "master_tables" || tableName == "master_columns" || tableName == "master_rules" || tableName == "master_rule_columns"))
            {
                throw new SimpleRDMSException($"Only SELECT-queries are allowed to {tableName}");
            }

            var table = database.GetTable(tableName);

            foreach(var subpart in query.Subparts)
            {
                if (subpart.Type == QueryPartType.AlterColumn) // Alter Column
                {
                    AlterColumn(tableName, subpart, database, results);
                }
                else if (subpart.Type == QueryPartType.AddColumnOrConstraint)
                {
                    var addColumnOrConstraint = subpart.Subparts.Single();
                    
                        if (addColumnOrConstraint.Type == QueryPartType.ColumnDefinition) // Add column
                    {
                        AddColumn(tableName, addColumnOrConstraint, database, results);
                    }
                    else if (addColumnOrConstraint.Type == QueryPartType.RuleDefinition) //Add rule
                    {
                        CreateTableRunner.AddRulesToMaster(addColumnOrConstraint, database, table, results);
                        results.Push(table.CreateUniqueConstraint(addColumnOrConstraint.Properties[QueryPropertyValue.RuleName], addColumnOrConstraint.Subparts.Where(q => q.Type == QueryPartType.ColumnName).Select(r => r.Properties[QueryPropertyValue.ColumnName]).ToArray()));
                    }
                    else
                    {
                        throw new SimpleRDMSInternalException($"Expected rule or column definition, got {addColumnOrConstraint.Type}");
                    }

                }
                else if (subpart.Type == QueryPartType.DropColumnOrRule)
                {
                    var dropColumnOrRule = subpart.Subparts.Single();

                    if (dropColumnOrRule.Type == QueryPartType.DropRule) //Drop constraint 
                    {
                        RemoveRuleFromMaster(dropColumnOrRule.Properties[QueryPropertyValue.RuleName], database, results);
                        results.Push(table.RemoveUniqueConstraint(dropColumnOrRule.Properties[QueryPropertyValue.RuleName]));
                    }
                    else if (dropColumnOrRule.Type == QueryPartType.DropColumn) //Drop column
                    {
                        RemoveColumnFromMaster(dropColumnOrRule.Properties[QueryPropertyValue.ColumnName], database, results);
                        results.Push(table.RemoveColumn(dropColumnOrRule.Properties[QueryPropertyValue.ColumnName]));
                    }

                }
            }

            return null;
        }

        static void AlterColumn(string tableName, QueryPart query, Database database, Stack<ResultBase> results)
        {
            var columnName = query.Properties[QueryPropertyValue.ColumnName];
            var action = query.Subparts.Single();

            if(action.Type == QueryPartType.SetNotNullable)
            {
                results.Push(database.GetTable(tableName).CreateNotNullConstraint(columnName));

                try
                {
                    UpdateQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"UPDATE master_columns SET nullable = 0 WHERE table_name = \"{tableName}\" AND column_name = \"{columnName}\""), database, results, true);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("UPDATE-operation to master_columns threw an exception.", e);
                }
            }
            else if(action.Type == QueryPartType.DropNotNullable)
            {
                results.Push(database.GetTable(tableName).RemoveNotNullConstraint(columnName));

                try
                {
                    UpdateQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"UPDATE master_columns SET nullable = 1 WHERE table_name = \"{tableName}\" AND column_name = \"{columnName}\""), database, results, true);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("UPDATE-operation to master_columns threw an exception.", e);
                }
            }
            else
            {
                throw new SimpleRDMSInternalException("Unknown action type: {action.Type}");
            }
        }

        static void AddColumn(string tableName, QueryPart query, Database database, Stack<ResultBase> results)
        {
            var column = CreateTableRunner.ParseColumn(query);
            var nullable = column.Nullable ? 1 : 0;
            results.Push(database.GetTable(tableName).CreateColumn(column));

            try
            {
                InsertQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"INSERT INTO master_columns (table_name, column_name, type, nullable) VALUES (\"{tableName}\", \"{column.Name}\", \"{column.Type}\", {nullable})"), database, results, true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("INSERT-operation to master_columns threw an exception.", e);
            }
        }

        static void RemoveRuleFromMaster(string constraintName, Database database, Stack<ResultBase> results)
        {
            try
            {
                DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_rules WHERE rule_name = \"{constraintName}\""), database, results, true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("DELETE-operation to master_rules threw an exception.", e);
            }

            try
            {
                DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_rule_columns WHERE rule_name = \"{constraintName}\""), database, results, true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("DELETE-operation to master_rule_columns threw an exception.", e);
            }
        }

        static void RemoveColumnFromMaster(string columnName, Database database, Stack<ResultBase> results)
        {
            var relatedConstraint = GetRelatedConstraint(database, columnName);
            
            if(relatedConstraint == null) // There are no unique constraints related to this column, so it can be dropped
            {
                try 
                { 
                    DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_columns WHERE column_name = \"{columnName}\""), database, results, true);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("DELETE-operation to master_columns threw an exception.", e);
                }
            }
            else
            {
                throw new SimpleRDMSException($"Cannot drop column {columnName}, as constraint {relatedConstraint["rule_name"]} depends on it.");
            }

        }

        static Row GetRelatedConstraint(Database database, string columnName)
        {
            try
            {
                return SelectQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"SELECT * FROM master_rule_columns WHERE column_name = \"{columnName}\""), database).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException($"Could not fetch constraint from master_rules related to column {columnName}.", e);
            }
        }


    }
}
