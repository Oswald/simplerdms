﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class DropTableRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results, bool allowMaster)
        {
            var tableName = query.Properties[Compiler.Enums.QueryPropertyValue.TableName];

            if (!allowMaster && (tableName == "master_tables" || tableName == "master_columns" || tableName == "master_rules" || tableName == "master_rule_columns"))
            {
                throw new SimpleRDMSException($"Only SELECT-queries are allowed to {tableName}");
            }

            var rulesOnTable = GetRulesOnTable(database, tableName);

            foreach (var rule in rulesOnTable)
            {
                try
                {
                    DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_rule_columns WHERE rule_name = \"{rule["rule_name"]}\""), database, results, true);
                }
                catch (Exception e)
                {
                    throw new SimpleRDMSInternalException("DELETE-operation to master_rule_columns threw an exception.", e);
                }
            }

            try
            {
                DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_rules WHERE table_name = \"{tableName}\""), database, results, true);
                DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_columns WHERE table_name = \"{tableName}\""), database, results, true);
                DeleteQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"DELETE FROM master_tables WHERE name = \"{tableName}\""), database, results, true);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("Deleting table, column or rule from master failed.", e);
            }

            results.Push(database.DropTable(tableName));

            return null;
        }

        static List<Row> GetRulesOnTable(Database database, string tableName)
        {
            try
            {
                return SelectQueryRunner.ExecuteQuery(QueryParser.ParseQuery($"SELECT * FROM master_rules WHERE table_name = \"{tableName}\""), database).ToList();
            }
            catch (Exception e)
            {
                throw new SimpleRDMSInternalException("Could not get rules from master_rules", e);
            }
        }
    }
}
