﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class WhereQueryRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, IEnumerable<Row> source)
        {
            return new WhereHandler(source.GetEnumerator(), GetCondition(query.Subparts, database));
        }

        public static ConditionStatementBase GetCondition(List<QueryPart> queryParts, Database database)
        {
            var statement = new List<Tuple<StatementPartType, object>>();
            AddQueryPartsToStatementList(queryParts, statement, database);

            try
            {
                while (statement.Count > 1)
                {
                    int highestPriorityConnective = 1;
                    
                    //Find the first AND. If does not exist, go with first OR.
                    for(int i = 1; i < statement.Count; i += 2)
                    {
                        var connective = statement[i];
                        if(connective.Item1 == StatementPartType.Connective)
                        {
                            if (connective.Item2.Equals("AND"))
                            {
                                highestPriorityConnective = i;
                                break;
                            }
                        }
                        else
                        {
                            throw new Exception($"Expected connective, but got {connective.Item1}");
                        }
                    }

                    var completedStatement = CreateCompletedStatement(statement[highestPriorityConnective - 1], (string)statement[highestPriorityConnective].Item2, statement[highestPriorityConnective + 1]);

                    statement.RemoveAt(highestPriorityConnective - 1);
                    statement.RemoveAt(highestPriorityConnective - 1);
                    statement[highestPriorityConnective - 1] = new Tuple<StatementPartType, object>(StatementPartType.CompletedCondition, completedStatement);


                }
            }
            catch (Exception e)
            {
                throw new Exception("Error when finding highest priority connective from WHERE-statement", e);
            }

            var term = statement.Single();

            if (term.Item1 == StatementPartType.CompletedCondition)
            {
                return (ConditionStatementBase)statement.Single().Item2;
            }
            else if(term.Item1 == StatementPartType.Condition)
            {
                return CreateComparison((QueryPart)term.Item2);
            }
            else
            {
                throw new Exception($"Statement list should not contain {term.Item1}");
            }
        }

        static WhereConnective CreateCompletedStatement(Tuple<StatementPartType, object> term1, string connective, Tuple<StatementPartType, object> term2)
        {
            return new WhereConnective(CreateConditionStatement(term1), (WhereStatementType)Enum.Parse(typeof(WhereStatementType), connective), CreateConditionStatement(term2));
        }

        static ConditionStatementBase CreateConditionStatement(Tuple<StatementPartType, object> term1)
        {
            if(term1.Item1 == StatementPartType.CompletedCondition)
            {
                return (ConditionStatementBase)term1.Item2;
            }
            else
            {
                return CreateComparison((QueryPart)term1.Item2);
            }
        }

        static WhereComparison CreateComparison(QueryPart part)
        {
            var comparisonOperator = GetOperator(part.Properties[QueryPropertyValue.Operator]);

            //TODO: Could we please get some optimization here?
            if (part.Properties.Count == 3)
            {

                object value;

                if (part.Properties.ContainsKey(QueryPropertyValue.Integer))
                {
                    value = Int32.Parse(part.Properties[QueryPropertyValue.Integer]);
                }
                else if(part.Properties.ContainsKey(QueryPropertyValue.String))
                {
                    value = part.Properties[QueryPropertyValue.String];
                }
                else
                {
                    throw new Exception("Statement should have contained a constant");
                }

                return new WhereComparison(Entities.ValueType.Constant, value, Entities.ValueType.ColumnName, part.Properties[QueryPropertyValue.ColumnName], comparisonOperator);
            }
            else if(part.Properties.Count == 2)
            {
                var latter = part.Subparts.Single();
                object latterValue;
                Entities.ValueType latterType;

                if (latter.Properties.ContainsKey(QueryPropertyValue.Integer))
                {
                    latterValue = Int32.Parse(latter.Properties[QueryPropertyValue.Integer]);
                    latterType = Entities.ValueType.Constant;
                }
                else if (latter.Properties.ContainsKey(QueryPropertyValue.String))
                {
                    latterValue = latter.Properties[QueryPropertyValue.String];
                    latterType = Entities.ValueType.Constant;
                }
                else if (latter.Properties.ContainsKey(QueryPropertyValue.ColumnName))
                {
                    latterValue = latter.Properties[QueryPropertyValue.ColumnName];
                    latterType = Entities.ValueType.ColumnName;
                }
                else if(latter.Properties.ContainsKey(QueryPropertyValue.Null))
                {
                    latterType = Entities.ValueType.Constant;
                    latterValue = null;
                }
                else
                {
                    throw new Exception("Could not handle the latter part of the comparison statement");
                }

                return new WhereComparison(Entities.ValueType.ColumnName, part.Properties[QueryPropertyValue.ColumnName], latterType, latterValue, comparisonOperator);
            }
            else
            {
                throw new Exception($"Expected either 2 or 3 query properties on condition, but got {part.Properties.Count}");
            }
        }

        static WhereFilterType GetOperator(string operatorValue)
        {
            switch (operatorValue)
            {
                case "=":
                    return WhereFilterType.Equal;
                case "!=":
                    return WhereFilterType.NotEqual;
                default:
                    throw new Exception($"Unknown operator: {operatorValue}");
            }
        }

        static void AddQueryPartsToStatementList(List<QueryPart> queryParts, List<Tuple<StatementPartType, object>> statement, Database database)
        {
            foreach (var part in queryParts)
            {
                if (part.Type == Compiler.Enums.QueryPartType.Condition)
                {
                    statement.Add(new Tuple<StatementPartType, object>(StatementPartType.Condition, part));
                }
                else if(part.Type == Compiler.Enums.QueryPartType.Delimiter)
                {
                    statement.Add(new Tuple<StatementPartType, object>(StatementPartType.Connective, part.Properties[Compiler.Enums.QueryPropertyValue.LogicalConnectiveType]));
                }
                else if(part.Type == Compiler.Enums.QueryPartType.ConditionBracket)
                {
                    statement.Add(new Tuple<StatementPartType, object>(StatementPartType.CompletedCondition, GetCondition(part.Subparts, database)));
                }
            }
        }

        enum StatementPartType
        {
            Connective,
            Condition,
            CompletedCondition
        }
    }
}
