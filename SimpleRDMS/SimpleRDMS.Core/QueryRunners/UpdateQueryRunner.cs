﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QuerySets;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryRunners
{
    static class UpdateQueryRunner
    {
        public static IEnumerable<Row> ExecuteQuery(QueryPart query, Database database, Stack<ResultBase> results, bool allowMaster)
        {
            var tableName = query.Properties[Compiler.Enums.QueryPropertyValue.TableName];

            if(!allowMaster && (tableName == "master_tables" || tableName == "master_columns" || tableName == "master_rules" || tableName == "master_rule_columns")) 
            {
                throw new SimpleRDMSException($"Only SELECT-queries are allowed to {tableName}");
            }

            var where = query.Subparts.SingleOrDefault(s => s.Type == Compiler.Enums.QueryPartType.Where);
            var handler = new UpdateHandler(new Entities.UpdateProperties(database.GetTable(tableName), GetColumnsAndValues(query), where == null ? null : WhereQueryRunner.GetCondition(where.Subparts, database)));
            handler.UpdateRows(results);
            return null;
        }

        static Dictionary<string, object> GetColumnsAndValues(QueryPart query)
        {
            return query.Subparts.Where(q => q.Type == Compiler.Enums.QueryPartType.UpdateColumnDefinition).ToDictionary(q => q.Properties[Compiler.Enums.QueryPropertyValue.ColumnName], q => InsertQueryRunner.ExecuteQueryQueryPartToValue(q.Subparts.Single()));
        }
    }
}
