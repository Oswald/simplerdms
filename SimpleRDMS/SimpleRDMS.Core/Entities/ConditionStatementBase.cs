﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public abstract class ConditionStatementBase
    {
        public abstract bool Satisfies(Row row);
    }
}
