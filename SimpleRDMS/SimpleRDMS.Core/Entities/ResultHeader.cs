﻿using SimpleRDMS.Buffer.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class ResultHeader
    {
        public List<DataType> Types { get; set; }
    }
}
