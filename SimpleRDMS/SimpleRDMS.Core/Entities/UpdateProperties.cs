﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class UpdateProperties
    {
        public Table Table { get; }
        public Dictionary<string, object> ColumnsAndValues { get; }
        public ConditionStatementBase Condition { get; }
        public UpdateProperties(Table table, Dictionary<string, object> columnsAndValues, ConditionStatementBase condition = null)
        {
            Table = table;
            ColumnsAndValues = columnsAndValues;
            Condition = condition ?? new WhereConnective();
        }
    }
}
