﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class InsertProperties
    {
        public Table Table { get; }
        public IEnumerable<object[]> Values { get; }
        public InsertProperties(Table table, IEnumerable<Row> values)
        {
            Table = table;
            Values = values.Select(v => Copy(v.Values));
        }

        object[] Copy(object[] values)
        {
            var clone = new object[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                clone[i] = values[i];
            }

            return clone;
        }

        public InsertProperties(Table table, IEnumerable<object[]> values)
        {
            Table = table;
            Values = values;
        }
    }
}
