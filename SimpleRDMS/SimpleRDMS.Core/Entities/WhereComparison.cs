﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public enum WhereFilterType
    {
        Equal,
        NotEqual
    }

    public enum ValueType
    {
        ColumnName,
        Constant
    }

    /// <summary>
    /// Class that actually checks comparison between two objects
    /// </summary>
    public class WhereComparison : ConditionStatementBase
    {
        public Func<Row, object> Value1 { get; set; }
        public Func<Row, object> Value2 { get; set; }
        public WhereFilterType Type { get; set; }
        public WhereComparison(ValueType value1Type, object value1, ValueType value2Type, object value2, WhereFilterType type)
        {
            Value1 = GetValueFunction(value1Type, value1);
            Value2 = GetValueFunction(value2Type, value2);
            Type = type;
        }

        Func<Row, object> GetValueFunction(ValueType type, object value)
        {
            switch (type)
            {
                case ValueType.Constant:
                    return (Row row) => value;
                case ValueType.ColumnName:
                    if(value is string str)
                    {
                        return (Row row) => row[str];
                    }
                    else
                    {
                        throw new Exception($"Invalid type for column name. Column name must be string.");
                    }
                default:
                    throw new Exception($"Unknown comparison value type: {type}");
            }
        }

        public override bool Satisfies(Row row)
        {
            var value1 = Value1(row);
            var value2 = Value2(row);

            switch (Type)
            {
                case WhereFilterType.Equal:
                    if (value1 == null)
                    {
                        return value2 == null;
                    }
                    else
                    {
                        return Value1(row).Equals(Value2(row));
                    }
                case WhereFilterType.NotEqual:
                    if (value1 == null)
                    {
                        return value2 != null;
                    }
                    else
                    {
                        return !Value1(row).Equals(Value2(row));
                    }
                default:
                    throw new Exception($"Unknown filter type: {Type}");
            }
        }
    }
}
