﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public enum WhereStatementType
    {
        AND,
        OR,
        Single,
        NoStatement,
    }

    /// <summary>
    /// Handles conditions.
    /// </summary>
    public class WhereConnective : ConditionStatementBase
    {
        WhereStatementType Type { get; }
        ConditionStatementBase Statement1 { get; }
        ConditionStatementBase Statement2 { get; }

        //Always true
        public WhereConnective()
        {
            Type = WhereStatementType.NoStatement;
        }
        public WhereConnective(WhereComparison filter)
        {
            Type = WhereStatementType.Single;
            Statement1 = filter;
            Statement2 = null;
        }

        public WhereConnective(ConditionStatementBase statement1, WhereStatementType type, ConditionStatementBase statement2)
        {
            Type = type;
            Statement1 = statement1;
            Statement2 = statement2;
        }

        public override bool Satisfies(Row row)
        {
            switch (Type)
            {
                case WhereStatementType.NoStatement:
                    return true;
                case WhereStatementType.Single:
                    return Statement1.Satisfies(row);
                case WhereStatementType.OR:
                    return Statement1.Satisfies(row) || Statement2.Satisfies(row);
                case WhereStatementType.AND:
                    return Statement1.Satisfies(row) && Statement2.Satisfies(row);
                default:
                    throw new Exception($"Unknown WhereStatementType: {Type}");
            }
        }
    }
}
