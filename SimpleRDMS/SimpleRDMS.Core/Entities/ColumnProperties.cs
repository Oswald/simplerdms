﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class ColumnProperties
    {
        public string ColumnName { get; set; }
        public string ColumnAlias { get; set; }
    }
}
