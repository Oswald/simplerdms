﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class SelectProperties
    {
        public string TableAlias { get; set; }
        public Table Table { get; set; } 
        public ColumnProperties[] Columns { get; set; }
        public SelectProperties(string tableAlias, Table table, ColumnProperties[] columns)
        {
            TableAlias = tableAlias;
            Table = table;
            Columns = columns;
        }
    }
}
