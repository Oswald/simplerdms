﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Core.QueryEnumerators;
using SimpleRDMS.Core.QueryHandlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class ResultRow : Row
    {
        public SelectEnumerator Selector { get; set; }

        public override string[] Columns => Selector.ColumnNames.ToArray();

        public ResultRow(SelectEnumerator selectEnumerator, object[] values)
        {
            Selector = selectEnumerator;
            Values = values;
        }
        public override object this[string columnName]
        {
            get
            {
                return Selector.GetValueByColumnName(this, columnName);
            }
        }
    }
}
