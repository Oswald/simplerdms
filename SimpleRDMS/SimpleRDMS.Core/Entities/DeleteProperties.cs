﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.Entities
{
    public class DeleteProperties
    {
        public Table Table { get; }
        public ConditionStatementBase Condition { get; }
        public DeleteProperties(Table table, ConditionStatementBase condition = null)
        {
            Table = table;
            Condition = condition ?? new WhereConnective();
        }
    }
}
