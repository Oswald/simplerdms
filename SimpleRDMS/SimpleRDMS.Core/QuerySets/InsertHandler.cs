﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.QuerySets
{
    public class InsertHandler
    {
        Table Table { get; }
        IEnumerable<object[]> Values { get; }

        public InsertHandler(InsertProperties properties)
        {
            Table = properties.Table;
            Values = properties.Values;
        }

        public void InsertRows(Stack<ResultBase> results)
        {
            foreach(var value in Values)
            {
                results.Push(Table.Insert(value));
            }
        }
    }
}
