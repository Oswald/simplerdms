﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryEnumerators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.QuerySets
{
    public class WhereHandler : IEnumerable<Row>
    {
        IEnumerator<Row> SelectEnumerator { get; }
        ConditionStatementBase Properties { get;}
        public WhereHandler(IEnumerator<Row> selectEnumerator, ConditionStatementBase properties)
        {
            SelectEnumerator = selectEnumerator;
            Properties = properties;
        }
        public IEnumerator<Row> GetEnumerator()
        {
            return new WhereEnumerator(SelectEnumerator, Properties);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new WhereEnumerator(SelectEnumerator, Properties);
        }
    }
}
