﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.QuerySets
{
    public class UpdateHandler
    {
        UpdateProperties Properties { get; }
        WhereHandler Where { get; }
        public UpdateHandler(UpdateProperties properties)
        {
            Properties = properties;
            Where = new WhereHandler(new RowIterator(properties.Table.First), properties.Condition);
        }

        public void UpdateRows(Stack<ResultBase> results)
        {
            foreach(DatabaseRow row in Where)
            {
                var clone = Copy(row.Values);

                foreach(var column in Properties.ColumnsAndValues)
                {
                    var index = Properties.Table.GetColumnIndex(column.Key);
                    clone[index] = column.Value;
                }

                results.Push(Properties.Table.Update(row, clone));
            }
        }

        object[] Copy(object[] values)
        {
            var clone = new object[values.Length];

            for(int i = 0; i < values.Length; i++)
            {
                clone[i] = values[i];
            }

            return clone;
        }
    }
}
