﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.QuerySets
{
    public class DeleteHandler
    {
        Table Table { get; }
        WhereHandler Where { get; }
        public DeleteHandler(DeleteProperties deleteProperties)
        {
            Table = deleteProperties.Table;
            Where = new WhereHandler(new RowIterator(Table.First), deleteProperties.Condition);
        }

        public void RemoveRows(Stack<ResultBase> results)
        {
            foreach(var row in Where)
            {
                results.Push(Table.Remove((DatabaseRow)row));
            }
        }
    }
}
