﻿using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryEnumerators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QuerySets
{
    public class SelectHandler : IEnumerable<ResultRow>
    {
        List<SelectProperties> Options { get; }
        public SelectHandler(List<SelectProperties> options)
        {
            Options = options;
        }
        public IEnumerator<ResultRow> GetEnumerator()
        {
            return new SelectEnumerator(Options);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
