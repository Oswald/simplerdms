﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Core.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Core.QueryEnumerators
{
    public class WhereEnumerator : IEnumerator<Row>
    {
        IEnumerator<Row> Selector { get; }
        ConditionStatementBase Properties { get; }
        public Row Current { get; set; } = null;
        object IEnumerator.Current => Current;
        public WhereEnumerator(IEnumerator<Row> selector, ConditionStatementBase properties)
        {
            Selector = selector;
            Properties = properties;
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            while (Selector.MoveNext())
            {
                if (Properties.Satisfies(Selector.Current))
                {
                    Current = Selector.Current;
                    return true;
                }
            }

            return false;
        }

        public void Reset()
        {
            Selector.Reset();
            Current = null;
        }
    }
}
