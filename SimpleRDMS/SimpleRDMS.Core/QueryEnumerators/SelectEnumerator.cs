﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Core.QueryEnumerators
{
    public class SelectEnumerator : IEnumerator<ResultRow>
    {
        List<SelectProperties> Properties { get; set; }

        private readonly List<IEnumerator<DatabaseRow>> RowIterators;
        public ResultRow Current { get; set; } = null;
        public List<string> ColumnNames { get; } = new List<string>();
        Dictionary<string, int> ColumnIndexByName { get; set; }
        public int RowLength { get; set; }
        

        object IEnumerator.Current => Current;

        public SelectEnumerator(List<SelectProperties> properties)
        {
            Properties = properties;

            var iterators = new List<IEnumerator<DatabaseRow>>();

            ColumnIndexByName = GetColumnIndices();

            foreach(var table in Properties)
            {
                var enumerator = table.Table.GetEnumerator();
                enumerator.MoveNext();
                iterators.Add(enumerator);

            }

            RowIterators = iterators;

        }

        void AddRowToResult(ResultRow result, DatabaseRow row, int index)
        {
            for(int i = 0; i < row.Values.Length; i++)
            {
                result.Values[i + index] = row.Values[i];
            }
        }

        public bool MoveNext()
        {
            var values = new object[RowLength];
            //TODO: Optimize by creating array of correct length in the beginning
            if (Current == null || AdvanceIterator(0))
            {
                for(int i = 0; i < RowIterators.Count; i++) 
                { 
                    var row = RowIterators[i].Current;

                    if(row == null) //Table is empty
                    {
                        return false;
                    }
                      
                    for(int j = 0; j < row.Values.Length; j++) 
                    {
                        var column = row.Columns[j];
                        var columnAlias = Properties[i].Columns.FirstOrDefault(f => f.ColumnName == column)?.ColumnAlias;
                        var table = Properties[i].TableAlias;

                        if (columnAlias != null && ColumnIndexByName.ContainsKey($"{table}.{columnAlias}"))
                        {
                            values[ColumnIndexByName[$"{table}.{columnAlias}"]] = row.Values[j];
                        }
                    }
                }

                Current = new ResultRow(this, values.ToArray());

                return true;
            }
            else
            {
                return false;
            }
        }

        bool AdvanceIterator(int iteratorIndex)
        {
            if (iteratorIndex == Properties.Count)
            {
                return false;
            }

            //If advancing this iterator is returns false, reset and advance next iterator. This loops through all row combinations if multiple rows were selected

            if (RowIterators[iteratorIndex].MoveNext())
            {
                return true;
            }
            else
            {
                RowIterators[iteratorIndex].Reset();
                return AdvanceIterator(iteratorIndex + 1);
            }
        }

        public void Reset()
        {
            foreach(var iterator in RowIterators)
            {
                iterator.Reset();
            }
        }

        public object GetValueByColumnName(ResultRow row, string columnName)
        {
            if (ColumnIndexByName.TryGetValue(columnName, out int index))
            {
                return row.Values[index];
            }
            else
            {
                throw new SimpleRDMSException($"Selection does not contain column {columnName}");
            }
        }

        Dictionary<string, int> GetColumnIndices()
        {
            var duplicates = Properties
                .GroupBy(p => p.TableAlias)
                .Where(g => g.Count() > 1);

            //If table with name "table" is selected multiple times, rename for selection: table1, table2
            foreach (var group in duplicates)
            {
                int duplicateIndex = 1;
                foreach (var table in group)
                {
                    table.TableAlias += duplicateIndex;
                    duplicateIndex += 1;
                }
            }

            var duplicateNamesOnColumns = Properties
                .SelectMany(p => p.Columns)
                .GroupBy(c => c.ColumnAlias)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key)
                .ToList();

            var index = 0;
            int rowLength = 0;
            var indices = new Dictionary<string, int>();

            foreach (var table in Properties)
            {
                foreach (var column in table.Columns)
                {
                    if(!table.Table.Columns.Any(c => c.Name == column.ColumnName))
                    {
                        throw new SimpleRDMSException($"Table {table.Table.Name} does not contain column {column.ColumnName}");
                    }

                    rowLength += 1;
                    //Access colum: WHERE tableName.columnName
                    indices.Add($"{table.TableAlias}.{column.ColumnAlias}", index);
                    if (!duplicateNamesOnColumns.Contains(column.ColumnAlias))
                    {
                        //Access colum: WHERE columnName
                        indices.Add(column.ColumnAlias, index);
                        ColumnNames.Add(column.ColumnAlias);
                    }
                    else
                    {
                        ColumnNames.Add($"{table.TableAlias}.{column.ColumnAlias}");
                    }
                    index += 1;
                }
            }

            RowLength = rowLength;

            return indices;
        }

        public void Dispose()
        {
            
        }
    }
}
