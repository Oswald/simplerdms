﻿using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public class Database
    {
        Dictionary<string, Table> Tables { get; set; }

        public Database(List<Table> tables)
        {
            Tables = tables.ToDictionary(t => t.Name, t => t);
        }

        public Table GetTable(string tableName)
        {
            if(Tables.TryGetValue(tableName, out Table t))
            {
                return t;
            }
            else
            {
                throw new SimpleRDMSException($"Table {tableName} does not exist");
            }
        }

        public ResultBase DropTable(string name)
        {
            var table = GetTable(name);
            Tables.Remove(name);
            return new DropTableResult(this, table);
        }

        public ResultBase AddTable(Table table)
        {
            if (Tables.ContainsKey(table.Name))
            {
                throw new SimpleRDMSException($"Table {table.Name} already exists");
            }
            else
            {
                Tables.Add(table.Name, table);
                return new CreateTableResult(this, table);
            }
        }

        public bool TableExists(string tableName)
        {
            return Tables.ContainsKey(tableName);
        }
    }
}
