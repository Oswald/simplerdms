﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public class DatabaseRow : Row
    {
        public override object this[string columnName]
        {
            get
            {
                var index = Table.GetColumnIndex(columnName);
                return Values[index];
            }
        }

        public Guid Id { get; set; } = Guid.NewGuid();
        public DatabaseRow Previous { get; set; }
        public DatabaseRow Next { get; set; }
        public Table Table { get; set; }

        public override string[] Columns => Table.Columns.Select(c => c.Name).ToArray();
    }
}
