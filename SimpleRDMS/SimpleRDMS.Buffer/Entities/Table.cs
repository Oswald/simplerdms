﻿using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public class Table : IEnumerable<DatabaseRow>
    {
        public string Name { get; set; }
        public Column[] Columns { get; set; }
        public DatabaseRow First { get; set; }
        public DatabaseRow Last { get; set; }
        public int Count { get; set; }

        public List<UniqueConstraintRule> UniqueConstraints { get; set; }

        public Table()
        {
            UniqueConstraints = new List<UniqueConstraintRule>();
            Columns = new Column[0];
            First = null;
            Last = null;
        }

        public InsertResult Insert(object[] values, Guid? rowId = null)
        {
            try
            {
                ValidateDataTypes(values);
                ValidateUniqueConstraint(values);

                var row = new DatabaseRow
                {
                    Previous = Last,
                    Values = values,
                    Next = null,
                    Table = this,
                };

                if(rowId != null)
                {
                    row.Id = rowId.Value;
                }

                if(First == null)
                {
                    First = row;
                    Last = row;
                }
                else
                {
                    Last.Next = row;
                }
                Last = row;

                foreach (var constraint in UniqueConstraints)
                {
                    constraint.Add(values, row.Id);
                }
                Count += 1;
                return new InsertResult(this, row);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not insert row to table {Name}.", e);
            }
        }

        /// <summary>
        /// Only for inserting previously removed row
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public InsertResult Insert(DatabaseRow row)
        {
            try
            {
                ValidateDataTypes(row.Values);
                ValidateUniqueConstraint(row.Values);

                //If DB is empty
                if (First == null)
                {
                    First = row;
                    Last = row;
                    row.Next = null;
                    row.Previous = null;
                }
                else
                {
                    //Check if was last element
                    if (row.Next == null)
                    {
                        Last.Next = row;
                        row.Previous = Last;
                        Last = row;
                    }
                    else
                    {
                        row.Next.Previous = row;
                    }
                    //Check if was first element
                    if(row.Previous == null)
                    {
                        First.Previous = row;
                        row.Next = First;
                        First = row;
                    }
                    else
                    {
                        row.Previous.Next = row;
                    }

                }
                foreach (var constraint in UniqueConstraints)
                {
                    constraint.Add(row.Values, row.Id);
                }
                Count += 1;
                return new InsertResult(this, row);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not insert row after another to table {Name}.", e);
            }
        }

        public UpdateResult Update(DatabaseRow existing, object[] values)
        {
            try
            {
                ValidateDataTypes(values);
                ValidateUniqueConstraint(values, existing.Id);

                var oldValues = existing.Values;
                existing.Values = values;

                foreach (var constraint in UniqueConstraints)
                {
                    constraint.Update(oldValues, values);
                }

                return new UpdateResult(this, existing, oldValues);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not insert row to table {Name}.", e);
            }
        }

        public RemoveResult Remove(DatabaseRow row)
        {
            try 
            {
                if (row.Previous == null)
                {
                    First = row.Next;
                }
                else
                {
                    row.Previous.Next = row.Next;
                }

                if (row.Next == null)
                {
                    Last = row.Previous;
                }
                else
                {
                    row.Next.Previous = row.Previous;
                }

                foreach(var constraint in UniqueConstraints)
                {
                    constraint.Remove(row.Values);
                }
                Count -= 1;
                return new RemoveResult(this, row);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not insert row to table {Name}.", e);
            }
        }

        public CreateColumnResult CreateColumn(Column column)
        {
            try
            {
                foreach(var existingColumn in Columns)
                {
                    if(existingColumn.Name == column.Name)
                    {
                        throw new SimpleRDMSException($"Column {column.Name} already exists");
                    }
                }

                Columns = AppendToArray(Columns, column, Columns.Length);

                foreach(var row in this)
                {
                    row.Values = AppendToArray(row.Values, null, row.Values.Length);
                }

                return new CreateColumnResult(this, column.Name);

            } 
            catch (Exception e)
            {
                throw new SimpleRDMSException("Could not create column", e);
            }
        }

        /// <summary>
        /// Use only for reverting transaction where column was removed
        /// </summary>
        /// <param name="column"></param>
        /// <param name="index"></param>
        public void CreateColumn(Column column, object[] columnValues, int index)
        {
            try
            {
                if(index > Columns.Length)
                {
                    throw new SimpleRDMSException("Index was greater than column count");
                };

                foreach (var existingColumn in Columns)
                {
                    if (existingColumn.Name == column.Name)
                    {
                        throw new SimpleRDMSException($"Column {column.Name} already exists");
                    }
                }

                Columns = AppendToArray(Columns, column, index);

                int i = 0;
                foreach (var row in this)
                {
                    row.Values = AppendToArray(row.Values, columnValues[i], index);
                    i += 1;
                }

            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not create column to table {Name}", e);
            }
        }

        T[] AppendToArray<T>(T[] oldArray, T element, int index)
        {
            var newArray = new T[oldArray.Length + 1];

            var x = 0;
            for (int i = 0; i < newArray.Length; i++)
            {
                if (i == index)
                {
                    x = 1;
                    newArray[i] = element;
                }
                else
                {
                    newArray[i] = oldArray[i - x];
                }

            }

            return newArray;
        }

        public RemoveColumnResult RemoveColumn(string name)
        {
            try
            {
                var columnIndex = GetColumnIndex(name);

                var column = Columns[columnIndex];
                Columns = RemoveFromArray(Columns, columnIndex);

                object[] columnValues = new object[Count];

                int rowIndex = 0;

                foreach(var row in this)
                {
                    columnValues[rowIndex] = row.Values[columnIndex];
                    row.Values = RemoveFromArray(row.Values, columnIndex);
                    rowIndex += 1;
                }

                return new RemoveColumnResult(this, columnValues, columnIndex, column);

            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not remove column {name} from table {Name}", e);
            }
        }

        public int GetColumnIndex(string name)
        {
            for (int i = 0; i < Columns.Length; i++)
            {
                if (Columns[i].Name == name)
                {
                    return i;
                }
            }

            throw new SimpleRDMSException($"Column {name} does not exist in table {Name}");
        }

        T[] RemoveFromArray<T>(T[] oldArray, int index)
        {
            var newArray = new T[oldArray.Length - 1];

            var x = 0;
            for (int i = 0; i < oldArray.Length; i++)
            {
                if (i == index)
                {
                    x = -1;
                }
                else
                {
                    newArray[x + i] = oldArray[i];
                }
            }

            return newArray;
        }

        public CreateUniqueRuleResult CreateUniqueConstraint(string constraintName, string[] columns)
        {
            try
            {
                var existing = GetUniqueConstraintByColumns(columns);
                if (existing != null)
                {
                    string columnString = "";
                    foreach (var column in columns)
                    {
                        columnString += column + " ";
                    }
                    throw new SimpleRDMSException($"Table already contains unique constraint with columns: {columnString}");
                }

                foreach (var column in columns)
                {
                    if(!Columns.Any(c => c.Name == column))
                    {
                        throw new SimpleRDMSException($"Column {column} does not exist in table {Name}");
                    }
                }

                var constraint = new UniqueConstraintRule(this, constraintName, columns);

                foreach(var row in this)
                {
                    constraint.Add(row.Values, row.Id);
                }

                UniqueConstraints.Add(constraint);

                return new CreateUniqueRuleResult(this, constraintName);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not add unique constraint to table {Name}", e);
            }
        }

        public RemoveUniqueRuleResult RemoveUniqueConstraint(string constraintName)
        {
            try
            {
                var existing = UniqueConstraints.FirstOrDefault(c => c.Name == constraintName);
                if (existing == null)
                {
                    throw new SimpleRDMSException($"Table does not contain unique constraint: {constraintName}");
                }

                UniqueConstraints.Remove(existing);

                return new RemoveUniqueRuleResult(this, existing.UniqueColumns, existing.Name);

            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not remove unique constraint from table {Name}", e);
            }
        }
        
        UniqueConstraintRule GetUniqueConstraintByColumns(string[] columns)
        {
            foreach (var constraint in UniqueConstraints)
            {
                if (constraint.UniqueColumns.Length == columns.Length && columns.All(c => constraint.UniqueColumns.Contains(c)))
                {
                    return constraint;
                }
            }

            return null;
        }
        
        public CreateNotNullResult CreateNotNullConstraint(string columnName)
        {
            try
            {
                var columnIndex = GetColumnIndex(columnName);
                if (!Columns[columnIndex].Nullable)
                {
                    throw new SimpleRDMSException($"Column already contains constraint");
                }

                foreach(var row in this)
                {
                    if(row.Values[columnIndex] == null)
                    {
                        throw new SimpleRDMSException("Column contains null values");
                    }
                }

                Columns[columnIndex].Nullable = false;

                return new CreateNotNullResult(this, columnName);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not create 'NOT NULL' -constraint to column {columnName}", e);
            }
        }

        public RemoveNotNullResult RemoveNotNullConstraint(string columnName)
        {
            try
            {
                var columnIndex = GetColumnIndex(columnName);
                if (Columns[columnIndex].Nullable)
                {
                    throw new SimpleRDMSException($"Column is already nullable");
                }

                Columns[columnIndex].Nullable = true;

                return new RemoveNotNullResult(this, columnName);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException($"Could not remove 'NOT NULL' -constraint from column {columnName}", e);
            }
        }

        void ValidateDataTypes(object[] values)
        {
            if(values.Length != Columns.Length)
            {
                throw new SimpleRDMSException($"Table has {Columns.Length}, but row has {values.Length} columns");
            }

            for(int i = 0; i < Columns.Length; i++)
            {
                if(values[i] != null && !Columns[i].Type.ValidateDataType(values[i]))
                {
                    throw new SimpleRDMSException($"Mismatching data type in column {Columns[i].Name}. Expected {Columns[i].Type}, but got {values[i].GetType()}");
                }
                else if(!Columns[i].Nullable && values[i] == null)
                {
                    throw new SimpleRDMSException($"Null constraint failed for column: {Columns[i].Name}");
                }
            }
        }

        void ValidateUniqueConstraint(object[] values, Guid? rowId = null)
        {
            foreach(var constraint in UniqueConstraints)
            {
                var isValid = constraint.ValidateRow(values, rowId);
                if (!isValid)
                {
                    string columns = "";
                    foreach(var columnName in constraint.UniqueColumns)
                    {
                        columns += columnName + " ";
                    }

                    throw new SimpleRDMSException($"Unique constraint failed for following columns(s): {columns}");
                }
            }
        }

        public IEnumerator<DatabaseRow> GetEnumerator()
        {
            return new RowIterator(First);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new RowIterator(First);
        }
    }
}
