﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    public class RowIterator : IEnumerator<DatabaseRow>
    {
        public DatabaseRow First { get; set; }
        public DatabaseRow Current { get; set; }
        public RowIterator(DatabaseRow first)
        {
            First = first;
            Current = null;
        }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            if(First == null)
            {
                return false;
            }
            else if(Current == null)
            {
                Current = First;
                return true;
            }
            else if(Current.Next == null)
            {
                return false;
            }
            else
            {
                Current = Current.Next;
                return true;
            }
        }

        public void Reset()
        {
            Current = First;
        }
    }
}
