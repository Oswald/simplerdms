﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public abstract class Row
    {
        public object[] Values { get; set; }
        public abstract string[] Columns { get; }
        public abstract object this[string columnName] { get; }

        public override string ToString()
        {
            var str = "[";

            for(int i = 0; i < Values.Length; i++)
            {
                if(Values[i] == null)
                {
                    str += "null";
                }
                else if(Values[i] is string)
                {
                    str += $"\"{Values[i]}\""; 
                }
                else
                {
                    str += Values[i];
                }

                if(i != Values.Length - 1)
                {
                    str += ", ";
                }
                else
                {
                    str += "]";
                }
            }

            return str;
        }
    }
}
