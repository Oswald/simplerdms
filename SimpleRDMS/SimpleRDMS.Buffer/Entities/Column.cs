﻿using SimpleRDMS.Buffer.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public class Column
    {
        public string Name { get; set; }
        public DataType Type { get; set; }
        public bool Nullable { get; set; }
    }
}
