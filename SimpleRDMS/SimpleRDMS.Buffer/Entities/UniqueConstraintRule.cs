﻿using SimpleRDMS.Buffer.Results;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Buffer.Entities
{
    [Serializable]
    public class UniqueConstraintRule
    {
        public string Name { get; }
        Dictionary<object, Guid> RowKeys { get; } = new Dictionary<object, Guid>(); 
        public string[] UniqueColumns { get; set; }
        Table Table { get; set; }

        public UniqueConstraintRule(Table table, string ruleName, string[] uniqueColumns)
        {
            UniqueColumns = uniqueColumns;
            Table = table;
            Name = ruleName;
        }

        //If unique key does not exist, or existing value is from this row
        public bool ValidateRow(object[] values, Guid? rowId)
        {
            var key = GetKey(values);
            return !RowKeys.ContainsKey(key) || (rowId != null && RowKeys[key] == rowId);
        }

        /// <summary>
        /// If a row is updated, there already exists an unique key for this row. If existing row has the same key as new one, value is ok.
        /// </summary>
        

        int GetUniqueColumnIndex(string columnName)
        {
            for(int i = 0; i < Table.Columns.Length; i++)
            {
                if(Table.Columns[i].Name == columnName)
                {
                    return i;
                }
            }
            throw new SimpleRDMSException($"Column {columnName} does not exist in table {Table.Name}");
        }

        public object GetKey(object[] values)
        {
            try
            {
                string key = "";

                if(UniqueColumns.Length == 0)
                {
                    throw new SimpleRDMSException("Cannot add unique constraint with 0 columns");
                }

                for (int i = 0; i < UniqueColumns.Length; i++)
                {
                    key += (values[GetUniqueColumnIndex(UniqueColumns[i])] + "\0");
                }
                return key;
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException("Could not generate uniqueness key", e);
            }
        }

        public void Add(object[] values, Guid rowId)
        {
            try
            {
                RowKeys.Add(GetKey(values), rowId);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException("Could not add key to unique constraint set", e);
            }
        }

        public void Update(object[] values, object[] newValues)
        {
            try
            {
                var id = Remove(values);
                Add(newValues, id);
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException("Could not update key to unique constraint set", e);
            }
        }

        public Guid Remove(object[] values)
        {
            try
            {
                var id = RowKeys[GetKey(values)];
                RowKeys.Remove(GetKey(values));
                return id;
            }
            catch (Exception e)
            {
                throw new SimpleRDMSException("Could not remove key from unique constraint set", e);
            }
        }
    }
}
