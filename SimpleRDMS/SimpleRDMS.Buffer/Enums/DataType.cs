﻿using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Enums
{
    public enum DataType
    {
        Integer,
        String
    }

    public static class DataTypeExtensions
    {
        public static bool ValidateDataType(this DataType dataType, object value)
        {
            switch (dataType)
            {
                case DataType.Integer:
                    if (value.GetType().Equals(typeof(int)))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case DataType.String:
                    if (value.GetType().Equals(typeof(string)))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    throw new SimpleRDMSException($"Unknown data type: {dataType}");

            }

        }
    }
}
