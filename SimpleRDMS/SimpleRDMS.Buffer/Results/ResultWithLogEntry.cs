﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public abstract class ResultWithLogEntry : ResultBase
    {
        public string TableName { get; }
        public Guid RowId { get; }
        public object[] Data { get; }
        public ResultWithLogEntry(string tableName, Guid rowId, object[] data)
        {
            HasLogEntry = true;
            TableName = tableName;
            RowId = rowId;
            Data = data;
        }
    }
}
