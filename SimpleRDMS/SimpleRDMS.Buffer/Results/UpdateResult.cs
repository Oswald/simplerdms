﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class UpdateResult : ResultWithLogEntry
    {
        Table Table { get; }
        DatabaseRow UpdatedRow { get; }
        object[] OldValues { get; set; }
        public UpdateResult(Table table, DatabaseRow updatedRow, object[] oldValues) : base(table.Name, updatedRow.Id, updatedRow.Values)
        {
            UpdatedRow = updatedRow;
            Table = table;
            OldValues = oldValues;
        }

        public override void Revert()
        {
            Table.Update(UpdatedRow, OldValues);
        }
    }
}
