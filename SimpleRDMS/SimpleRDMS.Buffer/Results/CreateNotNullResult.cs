﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class CreateNotNullResult : ResultBase
    {
        Table Table { get; }
        string Column { get; }
        public CreateNotNullResult(Table table, string column)
        {
            Table = table;
            Column = column;
        }

        public override void Revert()
        {
            Table.RemoveNotNullConstraint(Column);
        }
    }
}
