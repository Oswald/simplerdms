﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class RemoveUniqueRuleResult : ResultBase
    {
        Table Table { get; }
        string[] Columns { get; }
        string ConstraintName { get; }
        public RemoveUniqueRuleResult(Table table, string[] columns, string constraintName)
        {
            Table = table;
            Columns = columns;
            ConstraintName = constraintName;
        }

        public override void Revert()
        {
            Table.CreateUniqueConstraint(ConstraintName, Columns);
        }
    }
}
