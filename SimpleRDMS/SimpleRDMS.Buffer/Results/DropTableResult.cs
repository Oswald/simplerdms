﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class DropTableResult : ResultBase
    {

        public Database Database { get; }
        public Table Table { get; }

        public DropTableResult(Database database, Table table)
        {
            Database = database;
            Table = table;
        }
        public override void Revert()
        {
            Database.AddTable(Table);
        }
    }
}
