﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class InsertResult : ResultWithLogEntry
    {
        Table Table { get; }
        DatabaseRow InsertedRow { get; }
        public InsertResult(Table table, DatabaseRow insertedRow) : base(table.Name, insertedRow.Id, insertedRow.Values)
        {
            InsertedRow = insertedRow;
            Table = table;
        }

        public override void Revert()
        {
            Table.Remove(InsertedRow);
        }
    }
}
