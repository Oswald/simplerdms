﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public abstract class ResultBase
    {
        public bool HasLogEntry { get; set; } = false;
        public abstract void Revert();
    }
}
