﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class RemoveColumnResult : ResultBase
    {
        Table Table { get; set; }
        object[] Values { get; set; }
        int RemovedColumnIndex { get; set; }
        Column Column { get; set; }
        public RemoveColumnResult(Table table, object[] values, int removedColumnIndex, Column column)
        {
            Table = table;
            Values = values;
            RemovedColumnIndex = removedColumnIndex;
            Column = column;
        }

        public override void Revert()
        {
            Table.CreateColumn(Column, Values, RemovedColumnIndex);
        }
    }
}
