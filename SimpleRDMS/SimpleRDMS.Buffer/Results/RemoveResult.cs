﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class RemoveResult : ResultWithLogEntry
    {
        Table Table { get; }
        DatabaseRow RemovedRow { get; }
        public RemoveResult(Table table, DatabaseRow removedRow) : base(table.Name, removedRow.Id, null)
        {
            RemovedRow = removedRow;
            Table = table;
        }

        public override void Revert()
        {
            Table.Insert(RemovedRow);
        }
    }
}
