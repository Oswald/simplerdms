﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class CreateUniqueRuleResult : ResultBase
    {
        Table Table { get; }
        string Name { get; }
        public CreateUniqueRuleResult(Table table, string name)
        {
            Table = table;
            Name = name;
        }

        public override void Revert()
        {
            Table.RemoveUniqueConstraint(Name);
        }
    }
}
