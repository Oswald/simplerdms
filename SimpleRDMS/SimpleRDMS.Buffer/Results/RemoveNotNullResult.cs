﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class RemoveNotNullResult : ResultBase
    {
        public Table Table { get; }
        public string Column { get; }
        public RemoveNotNullResult(Table table, string column)
        {
            Table = table;
            Column = column;
        }

        public override void Revert()
        {
            Table.CreateNotNullConstraint(Column);
        }
    }
}
