﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Buffer.Results
{
    public class CreateColumnResult : ResultBase
    {
        Table Table { get; set; }
        string ColumnName { get; set; }
        public CreateColumnResult(Table table, string columnName)
        {
            Table = table;
            ColumnName = columnName;
        }

        public override void Revert()
        {
            Table.RemoveColumn(ColumnName);
        }
    }
}
