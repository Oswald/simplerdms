﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Domain.Exceptions
{
    public class SimpleRDMSInternalException : Exception
    {
        public SimpleRDMSInternalException(string message) : base(message)
        {

        }
        public SimpleRDMSInternalException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
