﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Domain.Exceptions
{
    public class SimpleRDMSException : Exception
    {
        public SimpleRDMSException(string message) : base(message)
        {

        }
        public SimpleRDMSException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
