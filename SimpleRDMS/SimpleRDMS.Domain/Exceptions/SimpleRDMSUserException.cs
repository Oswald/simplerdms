﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Domain.Exceptions
{
    public class SimpleRDMSUserException : Exception
    {
        public SimpleRDMSUserException(string message) : base(message)
        {

        }
        public SimpleRDMSUserException(string message, Exception innerException) : base (message, innerException)
        {

        }
    }
}
