﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class DeleteTests
    {
        [Fact]
        public void DeleteAllFromTable()
        {
            var query = QueryParser.ParseQuery("DELETE FROM Test");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")));

        }

        [Fact]
        public void DeleteAllFromTableWithCondition()
        {
            var query = QueryParser.ParseQuery("DELETE FROM Test WHERE int = 1");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Equal(4, queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")).Count());

        }

        [Fact]
        public void DeleteFromMaster_ThrowsException()
        {
            var query = QueryParser.ParseQuery("DELETE FROM master_tables");
            var queryRunner = TestEntities.GetQueryRunner();

            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(query));

        }
    }
}
