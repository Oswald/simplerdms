﻿using System;
using System.Collections.Generic;
using SimpleRDMS.Compiler.Logic;
using System.Text;
using Xunit;
using SimpleRDMS.Domain.Exceptions;

namespace SimpleRDMS.Tests.Integration
{
    public class DropTableTest
    {
        [Fact]
        public void DropTable()
        {
            var query = QueryParser.ParseQuery("DROP TABLE Test");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")));

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_tables")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));
        }

        [Fact]
        public void DropTable_DropRulesInTable()
        {
            var query = QueryParser.ParseQuery("DROP TABLE Test");
            var queryRunner = TestEntities.GetQueryRunner();
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test ADD CONSTRAINT UC_int UNIQUE (int)"));

            queryRunner.ExecuteQuery(query);

            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")));

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_tables")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));
        }
    }
}
