﻿using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class UpdateTests
    {
        [Fact]
        public void UpdateValues()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = 1");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.All(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")), row => row["int"].Equals(1));
        }

        [Fact]
        public void UpdateMultipleColumnValues()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = 1, str = \"1\"");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.All(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")), row => row["int"].Equals(1));
            Assert.All(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")), row => row["str"].Equals("1"));
        }

        [Fact]
        public void UpdateValuesToNull()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = null");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.True(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")).All(row => row["int"] == null));
        }

        [Fact]
        public void UpdateValueWithWhere()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = 100 WHERE int = 1");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Single(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")), row => row["int"].Equals(100));
        }
    }
}
