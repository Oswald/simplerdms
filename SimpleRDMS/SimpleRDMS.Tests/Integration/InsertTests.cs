﻿using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class InsertTests
    {
        [Fact]
        public void InsertValuesIntoTable()
        {
            var query = QueryParser.ParseQuery("INSERT INTO Test VALUES(1, \"1\")");
            var queryRunner = TestEntities.GetQueryRunnerForEmptyDb();

            queryRunner.ExecuteQuery(query);

            var rows = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")).ToList();

            Assert.Single(rows);
            Assert.Equal(1, rows.Single()["int"]);
            Assert.Equal("1", rows.Single()["str"]);
        }

        [Fact]
        public void InsertValuesWithColumnsSpecifiedIntoTable()
        {
            var query = QueryParser.ParseQuery("INSERT INTO Test (str) VALUES(\"1\")");
            var queryRunner = TestEntities.GetQueryRunnerForEmptyDb();

            queryRunner.ExecuteQuery(query);

            var rows = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")).ToList();

            Assert.Single(rows);
            Assert.Null(rows.Single()["int"]);
            Assert.Equal("1", rows.Single()["str"]);
        }

        [Fact]
        public void InsertValuesWithColumnsSpecifiedIntoTable_2()
        {
            var query = QueryParser.ParseQuery("INSERT INTO Test (str, int) VALUES(\"1\", 1)");
            var queryRunner = TestEntities.GetQueryRunnerForEmptyDb();

            queryRunner.ExecuteQuery(query);

            var rows = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM Test")).ToList();

            Assert.Single(rows);
            Assert.Equal(1, rows.Single()["int"]);
            Assert.Equal("1", rows.Single()["str"]);
        }
    }
}
