﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Recovery.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class LogWriterTests
    {
        static LogEntry ParseLogEntry(string line)
        {
            return System.Text.Json.JsonSerializer.Deserialize<LogEntry>(line);
        }

        [Fact]
        public void CheckThatLogIsWrittenOnInsertUpdateAndDelete()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner = tuple.Item1;
            var logs = tuple.Item2;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES (1, \"1\")"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("UPDATE Test SET int = 10 WHERE int = 1"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("DELETE FROM Test WHERE int = 10"));

            Assert.Equal(5, logs.Lines.Count);

            var e1 = ParseLogEntry(logs.Lines[0]);
            var e2 = ParseLogEntry(logs.Lines[1]);
            var e3 = ParseLogEntry(logs.Lines[2]);
            var e4 = ParseLogEntry(logs.Lines[3]);
            var e5 = ParseLogEntry(logs.Lines[4]);

            Assert.Equal(e1.Data.Id, e3.Data.Id);
            Assert.Equal(e1.Data.Id, e5.Data.Id);
            Assert.Equal(e2.Data.Id, e4.Data.Id);

            Assert.NotNull(e1.Data.Row);
            Assert.NotNull(e2.Data.Row);
            Assert.NotNull(e3.Data.Row);
            Assert.Null(e4.Data.Row);
            Assert.Null(e5.Data.Row);
        }

        [Fact]
        public void CheckThatLogIsWrittenOnCreateTable()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner = tuple.Item1;
            var logs = tuple.Item2;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));

            Assert.Equal(5, logs.Lines.Count);

            var e1 = ParseLogEntry(logs.Lines[0]);
            var e2 = ParseLogEntry(logs.Lines[1]);
            var e3 = ParseLogEntry(logs.Lines[2]);
            var e4 = ParseLogEntry(logs.Lines[3]);
            var e5 = ParseLogEntry(logs.Lines[4]);

            Assert.Equal("master_tables", e1.Data.TableName);
            Assert.Equal("master_columns", e2.Data.TableName);
            Assert.Equal("master_columns", e3.Data.TableName);
            Assert.Equal("master_rules", e4.Data.TableName);
            Assert.Equal("master_rule_columns", e5.Data.TableName);
        }

        [Fact]
        public void WriteLogWhenTransactionCommited()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner = tuple.Item1;
            var logs = tuple.Item2;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES (1, \"1\")"));

            Assert.Empty(logs.Lines);

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("COMMIT"));
            Assert.Single(logs.Lines);
        }

        [Fact]
        public void DoNotWriteLogWhenTransactionRolledBack()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner = tuple.Item1;
            var logs = tuple.Item2;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES (1, \"1\")"));

            Assert.Empty(logs.Lines);

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ROLLBACK"));
            Assert.Empty(logs.Lines);
        }
    }
}
