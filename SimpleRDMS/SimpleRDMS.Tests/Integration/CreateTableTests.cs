﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class CreateTableTests
    {
        [Fact]
        public void CreateTableWithTwoColumns()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str)");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM t1")));

            var columns = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns"));

            Assert.Single(columns.Where(c => c["column_name"].Equals("int") && c["table_name"].Equals("t1") && c["type"].Equals("Integer") && c["nullable"].Equals(0)));
            Assert.Single(columns.Where(c => c["column_name"].Equals("str") && c["table_name"].Equals("t1") && c["type"].Equals("String") && c["nullable"].Equals(1)));

            var tables = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_tables"));
            Assert.Equal(2, tables.Count());
            Assert.Contains(tables, t => t["name"].Equals("t1"));
        }

        [Fact]
        public void CreateTableWithTwoColumns_InsertingWorks()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str)");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(1, \"1\")"));
            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(null, \"1\")")));
        }

        [Fact]
        public void CreateTableWithTwoColumnsAndUniqueConstraint()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueIntStr UNIQUE(int, str))");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM t1")));

            var rules = queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns"));
            Assert.NotEmpty(rules);
        }

        [Fact]
        public void CreateTableWithTwoColumnsAndUniqueConstraint_InsertingWorks()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueIntStr UNIQUE(int, str))");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(1, \"1\")"));
            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(1, \"1\")")));
        }

    }
}
