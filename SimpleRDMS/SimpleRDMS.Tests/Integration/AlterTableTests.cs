﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class AlterTableTests
    {
        /*
        ALTER TABLE table_name ALTER COLUMN column_name SET NOT NULL;
        ALTER TABLE table_name ALTER COLUMN column_name DROP NOT NULL;

        ALTER TABLE table_name ADD COLUMN column_name datatype; 
        ALTER TABLE table_name DROP COLUMN column_name; 

        ALTER TABLE table_name ADD CONSTRAINT UC_Person UNIQUE (ID,LastName); 
        ALTER TABLE table_name DROP CONSTRAINT UC_Person; 
        */

        [Fact]
        public void SetColumnNotNull()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE Test ALTER COLUMN int SET NOT NULL");
            var queryRunner = TestEntities.GetQueryRunner();
            //Just ensure, that there already is one not nullable column
            Assert.Single(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns WHERE nullable = 0")));

            queryRunner.ExecuteQuery(query);

            //Test that adding new row works, but adding null row throws exception
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test (int, str) VALUES (1, \"asd\")"));
            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test (int, str) VALUES (null, \"asd\")")));

            //Both are supposed to be unique
            Assert.Equal(2, queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns WHERE nullable = 0")).Count());
        }

        [Fact]
        public void SetColumnToNullable()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE Test ALTER COLUMN str DROP NOT NULL");
            var queryRunner = TestEntities.GetQueryRunner();


            queryRunner.ExecuteQuery(query);

            //Now there should not be columns with NOT NULL
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns WHERE nullable = 0")));
            //Test that adding null works
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test (int, str) VALUES (null, null)"));
        }

        [Fact]
        public void AddColumn()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE Test ADD COLUMN new_column INTEGER");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            //Verify that there are 3 columns now
            Assert.Equal(3, queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns")).Count());
            //Verify that inserting works
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES (6, \"6\", 6)"));
        }

        [Fact]
        public void AddUniqueConstraint()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE Test ADD CONSTRAINT UC_int UNIQUE (int)");
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(query);

            Assert.Single(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns WHERE column_name = \"int\"")));

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(7, \"7\")"));
            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(7, \"7\")")));
        }

        [Fact]
        public void DropUniqueConstraint()
        {
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test ADD CONSTRAINT UC_int UNIQUE (int)"));

            Assert.NotEmpty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rules")));
            Assert.NotEmpty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test DROP CONSTRAINT UC_int"));

            //This would throw an exception, if constraint was not deleted
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(7, \"7\")"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(7, \"7\")"));

            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rules")));
            Assert.Empty(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_rule_columns")));
        }

        [Fact]
        public void DropColumn()
        {
            var queryRunner = TestEntities.GetQueryRunner();
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test DROP COLUMN int"));

            Assert.Single(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns")));
            //This should work now, as there is only one column
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(\"7\")"));
        }

        [Fact]
        public void DropColumnWhenThereIsExistingRule()
        {
            var queryRunner = TestEntities.GetQueryRunner();

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test ADD CONSTRAINT UC_str UNIQUE (str)"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test DROP COLUMN int"));

            Assert.Single(queryRunner.ExecuteQuery(QueryParser.ParseQuery("SELECT * FROM master_columns")));
            //This should work now, as there is only one column
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO Test VALUES(\"7\")"));
        }

        [Fact]
        public void DropColumnWithConstraint_ThrowsException()
        {
            var queryRunner = TestEntities.GetQueryRunner();
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test ADD CONSTRAINT UC_int UNIQUE (int)"));
            Assert.Throws<SimpleRDMSUserException>(() => queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE Test DROP COLUMN int")));
        }
    }
}
