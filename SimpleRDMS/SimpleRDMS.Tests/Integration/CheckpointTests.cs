﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Recovery.Interfaces;
using SimpleRDMS.Recovery.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class CheckpointTests
    {
        [Fact]
        public void CheckpointIsCreatedCorrectly_CreateTable()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_CreateAndDropTable()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("DROP TABLE t1"));
            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_DropConstraint()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN str, DROP CONSTRAINT uniqueInt"));

            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_AddColumnAndConstraint()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN str, DROP CONSTRAINT uniqueInt"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD COLUMN asd STRING"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD CONSTRAINT asd UNIQUE(asd)"));

            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_DropNewlyAddedConstraint()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN str, DROP CONSTRAINT uniqueInt"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD COLUMN asd STRING"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD CONSTRAINT asd UNIQUE(asd)"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP CONSTRAINT asd"));

            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_InsertData()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN str, DROP CONSTRAINT uniqueInt"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD COLUMN asd STRING"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD CONSTRAINT asd UNIQUE(asd)"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP CONSTRAINT asd"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 (int, asd) VALUES(9, \"9\")"));

            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }

        [Fact]
        public void CheckpointIsCreatedCorrectly_WithExistingCheckpoint()
        {
            var tuple = TestEntities.GetQueryRunnerWithLogInterfaceAndDb(false);
            var checkpointCreator = new CheckpointCreator(tuple.Item2, new ListInterface(), tuple.Item4);
            var queryRunner = tuple.Item1;

            queryRunner.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN str, DROP CONSTRAINT uniqueInt"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD COLUMN asd STRING"));
            checkpointCreator.CreateCheckpoint();
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD CONSTRAINT asd UNIQUE(asd)"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP CONSTRAINT asd"));
            queryRunner.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 (int, asd) VALUES(9, \"9\")"));

            var checkpoint = checkpointCreator.CreateCheckpoint();

            DatabaseValidator.AreEqual(tuple.Item3, checkpoint, "t1");
        }
    }
}
