﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Tests.Buffer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    static class DatabaseValidator
    {
        public static void AreEqual(Database db1, Database db2, string additionalTableName = null)
        {
            var tables = new List<string>
            {
                "master_tables",
                "master_columns",
                "master_rules",
                "master_rule_columns"
            };

            if (additionalTableName != null)
            {
                tables.Add(additionalTableName);
            }

            foreach (var table in tables)
            {
                try
                {
                    if (db1.TableExists(table) && db2.TableExists(table))
                    {

                        var t1 = db1.GetTable(table);
                        var t2 = db2.GetTable(table);

                        BufferValidator.AreEqual(BufferValidator.ValidateTable(t1), BufferValidator.ValidateTable(t2));

                        //Check that columns are equal
                        for (int i = 0; i < t1.Columns.Length; i++)
                        {
                            Assert.Equal(t1.Columns[i].Type, t2.Columns[i].Type);
                            Assert.Equal(t1.Columns[i].Name, t2.Columns[i].Name);
                            Assert.Equal(t1.Columns[i].Nullable, t2.Columns[i].Nullable);
                        }

                        //Check that constraints are equal
                        for (int i = 0; i < t1.UniqueConstraints.Count; i++)
                        {
                            Assert.Equal(t1.UniqueConstraints[i].Name, t2.UniqueConstraints[i].Name);

                            for (int j = 0; j < t1.UniqueConstraints[i].UniqueColumns.Length; j++)
                            {
                                Assert.Equal(t1.UniqueConstraints[i].UniqueColumns[j], t2.UniqueConstraints[i].UniqueColumns[j]);
                            }
                        }
                    }
                    else if(db1.TableExists(table) != db2.TableExists(table))
                    {
                        throw new Exception($"Only another database had table {table}");
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Table {table} was not identical on databases, or was not valid", e);
                }
            }
        }
    }
}
