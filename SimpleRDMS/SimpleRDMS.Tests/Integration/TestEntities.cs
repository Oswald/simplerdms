﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Recovery.Interfaces;
using SimpleRDMS.Recovery.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SimpleRDMS.Tests.Integration
{
    static class TestEntities
    {
        public static Database GetDatabase()
        {
            var table = Buffer.TestEntities.GetTableWithTwoColumns();

            foreach (var row in Buffer.TestEntities.GetFiveRows())
            {
                table.Insert(row);
            }

            var tables = new List<Table> { table };

            var masterTables = new Table
            {
                Name = "master_tables",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "name",
                        Nullable = false,
                        Type = DataType.String
                    }
                }
            };

            masterTables.CreateUniqueConstraint("table_name_is_unique", new string[] { "name" });


            var masterColumns = new Table
            {
                Name = "master_columns",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "table_name",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "column_name",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "type",
                        Nullable = false,
                        Type = DataType.String
                    },
                    new Column
                    {
                        Name = "nullable",
                        Nullable = false,
                        Type = DataType.Integer
                    }
                }
            };

            masterColumns.CreateUniqueConstraint("column_name_is_unique_within_table", new string[] { "column_name", "table_name" });

            var masterRules = new Table
            {
                Name = "master_rules",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "table_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "rule_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "rule_type",
                        Type = DataType.String,
                        Nullable = false
                    }
                }
            };

            masterRules.CreateUniqueConstraint("rule_name_is_unique", new string[] { "rule_name" });

            var masterRuleColumns = new Table
            {
                Name = "master_rule_columns",
                Columns = new Column[]
                {
                    new Column
                    {
                        Name = "rule_name",
                        Type = DataType.String,
                        Nullable = false
                    },
                    new Column
                    {
                        Name = "column_name",
                        Type = DataType.String,
                        Nullable = false
                    }
                }
            };

            masterTables.Insert(new object[] { "Test" });
            masterColumns.Insert(new object[] { "Test", "int", "Integer", 1 });
            masterColumns.Insert(new object[] { "Test", "str", "String", 0 });

            masterRuleColumns.CreateUniqueConstraint("colum_can_belong_to_rule_only_once", new string[] { "rule_name", "column_name" });

            tables.Add(masterTables);
            tables.Add(masterColumns);
            tables.Add(masterRules);
            tables.Add(masterRuleColumns);

            return new Database(tables);
        }
        public static QueryRunner GetQueryRunner()
        {
            return new QueryRunner(GetDatabase(), new LogWriter(new ListInterface()), new Mutex());
        }

        public static Tuple<QueryRunner, ListInterface, Database, Mutex> GetQueryRunnerWithLogInterfaceAndDb(bool useDebugDatabase = true)
        {
            var logInterface = new ListInterface();
            var db = useDebugDatabase ? GetDatabase() : DatabaseInitializer.InitializeDatabase();
            var mutex = new Mutex();
            return new Tuple<QueryRunner, ListInterface, Database, Mutex>(new QueryRunner(db, new LogWriter(logInterface), mutex), logInterface, db, mutex);
        }

        public static QueryRunner GetQueryRunnerForEmptyDb()
        {
            var table = Buffer.TestEntities.GetTableWithTwoColumns();
            return new QueryRunner(new Database(new List<Table> { table }), new LogWriter(new ListInterface()), new Mutex());
        }



    }
}
