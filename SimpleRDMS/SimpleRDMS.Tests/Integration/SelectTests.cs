﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Compiler.Logic.Parsers;
using SimpleRDMS.Core.QueryRunners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class SelectTests
    {
        [Fact]
        public void SelectColumnFromTableWithAliasesAndWhereWithComplexCondition()
        {
            var query = QueryParser.ParseQuery("SELECT int FROM Test WHERE int = 1 AND (int = 1 OR 1 = int)");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Single(rows);
            Assert.Equal(1, rows.Single()["int"]);
        }

        [Fact]
        public void SelectColumnFromTableWithAliasesAndWhere()
        {
            var query = QueryParser.ParseQuery("SELECT int AS c FROM Test WHERE c = 1");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Single(rows);
            Assert.Equal(1, rows.Single()["c"]);
        }

        [Fact]
        public void SelectWhereNotEqual()
        {
            var query = QueryParser.ParseQuery("SELECT int AS c FROM Test WHERE c != 1");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Equal(4, rows.Count());
        }

        [Fact]
        public void SelectWhereNull()
        {
            var query = QueryParser.ParseQuery("SELECT int AS c FROM Test WHERE c = null");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Empty(rows);
        }

        [Fact]
        public void SelectAllFromTable()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM Test");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Equal(5, rows.Count);
        }

        [Fact]
        public void SelectAllFromTwoTables()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM Test AS T1, * FROM Test AS T2");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Equal(25, rows.Count);
        }

        [Fact]
        public void SelectSpecificFromTwoTables()
        {
            var query = QueryParser.ParseQuery("SELECT int, str FROM Test AS T1, int, str FROM Test AS T2");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Equal(25, rows.Count);
            Assert.Equal(4, rows.First().Columns.Count());
            Assert.Equal(4, rows.First().Values.Count());
        }

        [Fact]
        public void SelectAllFromTwoTablesWithComplexCondition()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM Test AS T1, * FROM Test AS T2 WHERE (T1.int = T2.int) OR (T1.str = \"1\")");
            var queryRunner = TestEntities.GetQueryRunner();

            var rows = queryRunner.ExecuteQuery(query).ToList();

            Assert.Equal(9, rows.Count);
        }
    }
}
