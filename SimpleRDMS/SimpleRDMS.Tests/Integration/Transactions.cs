﻿using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Tests.Buffer;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Integration
{
    public class Transactions
    {
        [Fact]
        public void CheckThatTransactionsProduceIdenticalResult1()
        {
            var tuple1 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner1 = tuple1.Item1;
            var db1 = tuple1.Item3;

            var tuple2 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner2 = tuple2.Item1;
            var db2 = tuple2.Item3;

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner2.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(10, \"10\")"));
            queryRunner2.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(10, \"10\")"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("COMMIT"));

            DatabaseValidator.AreEqual(db1, db2, "t1");
        }

        [Fact]
        public void CheckThatTransactionsProduceIdenticalResultOnDropTable1()
        {
            var tuple1 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner1 = tuple1.Item1;
            var db1 = tuple1.Item3;

            var tuple2 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var db2 = tuple2.Item3;

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("DROP TABLE t1"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ROLLBACK"));

            DatabaseValidator.AreEqual(db1, db2, "t1");
        }

        [Fact]
        public void CheckThatTransactionsProduceIdenticalResultOnDropTable2()
        {
            var tuple1 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner1 = tuple1.Item1;
            var db1 = tuple1.Item3;

            var tuple2 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var db2 = tuple2.Item3;
            var queryRunner2 = tuple2.Item1;

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner2.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("DROP TABLE t1"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ROLLBACK"));

            DatabaseValidator.AreEqual(db1, db2, "t1");
        }

        [Fact]
        public void CheckThatTransactionsProduceIdenticalResult2()
        {
            var tuple1 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner1 = tuple1.Item1;
            var db1 = tuple1.Item3;

            var tuple2 = TestEntities.GetQueryRunnerWithLogInterfaceAndDb();
            var queryRunner2 = tuple2.Item1;
            var db2 = tuple2.Item3;

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));
            queryRunner2.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int NOT NULL, STRING str, CONSTRAINT uniqueInt UNIQUE(int))"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("CREATE TABLE t2(INTEGER int NOT NULL, STRING str)"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(9, \"9\")"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP CONSTRAINT uniqueInt"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD COLUMN asd INTEGER"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 DROP COLUMN int"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ALTER TABLE t1 ADD CONSTRAINT asd UNIQUE(asd)"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("DELETE FROM t1"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("DROP TABLE t1"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("ROLLBACK"));

            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("BEGIN TRANSACTION"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(10, \"10\")"));
            queryRunner1.ExecuteQuery(QueryParser.ParseQuery("COMMIT"));

            queryRunner2.ExecuteQuery(QueryParser.ParseQuery("INSERT INTO t1 VALUES(10, \"10\")"));

            DatabaseValidator.AreEqual(db1, db2, "t1");
        }
    }
}
