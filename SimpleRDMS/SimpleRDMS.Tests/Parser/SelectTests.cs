﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class SelectTests
    {
        [Fact]
        public void ParseSelect()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM asd");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);

            Assert.Empty(from.Subparts);
            Assert.Equal("*", from.Properties[QueryPropertyValue.ColumnName]);


        }

        [Fact]
        public void ParseSelectWithColumnNames()
        {
            var query = QueryParser.ParseQuery("SELECT column, column2 FROM asd");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);

            Assert.Equal(2, from.Subparts.Count);

            var column = from.Subparts[0];
            Assert.Equal("column", column.Properties[QueryPropertyValue.ColumnName]);

            var column2 = from.Subparts[1];
            Assert.Equal("column2", column2.Properties[QueryPropertyValue.ColumnName]);


        }

        [Fact]
        public void ParseSelectWithColumnNameAndAlias()
        {
            var query = QueryParser.ParseQuery("SELECT column AS c FROM asd AS t1");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);
            Assert.Equal("t1", from.Properties[QueryPropertyValue.Alias]);

            Assert.Single(from.Subparts);

            var column = from.Subparts[0];
            Assert.Equal("column", column.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("c", column.Properties[QueryPropertyValue.Alias]);

        }

        [Fact]
        public void ParseSelectWithWhereColumnEqualsInteger()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE asd = 1");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var integer = statement.Subparts.Single();

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);

        }

        [Fact]
        public void ParseSelectWithWhereColumnEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE asd = asd2");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var column = statement.Subparts.Single();

            Assert.Equal(QueryPartType.ColumnName, column.Type);
            Assert.Equal("asd2", column.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = asd");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("1", statement.Properties[QueryPropertyValue.Integer]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereStringEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE \"1\" = asd");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("1", statement.Properties[QueryPropertyValue.String]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsInteger_ThrowsException()
        {
            Assert.Throws<SimpleRDMSException>(() => QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = 1"));
        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsString_ThrowsException()
        {
            Assert.Throws<SimpleRDMSException>(() => QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = \"1\""));
        }

        [Fact]
        public void ParseWhereWithAnd()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE \"1\" = asd AND asd = null OR 1 = asd");
            var where = query.Subparts[1];

            Assert.Equal(5, where.Subparts.Count);

            var first = where.Subparts[0];
            var second = where.Subparts[2];
            var third = where.Subparts[4];

            var and = where.Subparts[1];
            var or = where.Subparts[3];

            Assert.Equal(QueryPartType.Delimiter, and.Type);
            Assert.Equal(QueryPartType.Delimiter, or.Type);

            Assert.Equal("AND", and.Properties[QueryPropertyValue.LogicalConnectiveType]);
            Assert.Equal("OR", or.Properties[QueryPropertyValue.LogicalConnectiveType]);

            Assert.Equal("1", first.Properties[QueryPropertyValue.String]);
            Assert.Equal("=", first.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", first.Properties[QueryPropertyValue.ColumnName]);

            Assert.Equal(QueryPartType.Condition, second.Type);
            Assert.Equal("asd", second.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", second.Properties[QueryPropertyValue.Operator]);

            Assert.Single(second.Subparts);
            var nil = second.Subparts.Single();

            Assert.Equal(QueryPartType.Null, nil.Type);


            Assert.Equal(QueryPartType.Condition, third.Type);
            Assert.Equal("1", third.Properties[QueryPropertyValue.Integer]);
            Assert.Equal("=", third.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", third.Properties[QueryPropertyValue.ColumnName]);
        }

        [Fact]
        public void ParseWhereWithBrackets()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE (\"1\" = asd AND asd = null) OR 1 = asd");

            var bracket = query.Subparts[1].Subparts.First();
            Assert.Equal(QueryPartType.ConditionBracket, bracket.Type);
        }
    }
}
