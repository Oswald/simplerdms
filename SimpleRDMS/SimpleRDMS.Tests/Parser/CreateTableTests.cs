﻿using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class TableParserTests
    {
        [Fact]
        public void ParseTable()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int, STRING str)");

            Assert.Equal(QueryPartType.CreateTable, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[0].Type);
            Assert.Equal("int", query.Subparts[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("INTEGER", query.Subparts[0].Properties[QueryPropertyValue.DataType]);

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[1].Type);
            Assert.Equal("str", query.Subparts[1].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("STRING", query.Subparts[1].Properties[QueryPropertyValue.DataType]);

        }

        [Fact]
        public void ParseTableWithRules()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int, STRING str NOT NULL, CONSTRAINT u UNIQUE(int, str))");

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[0].Type);
            Assert.Equal("int", query.Subparts[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("INTEGER", query.Subparts[0].Properties[QueryPropertyValue.DataType]);

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[1].Type);
            Assert.Equal("str", query.Subparts[1].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("STRING", query.Subparts[1].Properties[QueryPropertyValue.DataType]);
            Assert.True(query.Subparts[1].Properties.ContainsKey(QueryPropertyValue.NotNullable));

            Assert.Equal(QueryPartType.RuleDefinition, query.Subparts[2].Type);
            var ruleTables = query.Subparts[2].Subparts;
            Assert.Equal(QueryPartType.ColumnName, ruleTables[0].Type);
            Assert.Equal(QueryPartType.ColumnName, ruleTables[1].Type);

            Assert.Equal("int", ruleTables[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("str", ruleTables[1].Properties[QueryPropertyValue.ColumnName]);
        }

        [Fact]
        public void ParseTableWithRuleNameSpecified()
        {
            var query = QueryParser.ParseQuery("CREATE TABLE t1(INTEGER int, STRING str NOT NULL, CONSTRAINT uniqueIntStr UNIQUE(int, str))");

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[0].Type);
            Assert.Equal("int", query.Subparts[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("INTEGER", query.Subparts[0].Properties[QueryPropertyValue.DataType]);

            Assert.Equal(QueryPartType.ColumnDefinition, query.Subparts[1].Type);
            Assert.Equal("str", query.Subparts[1].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("STRING", query.Subparts[1].Properties[QueryPropertyValue.DataType]);
            Assert.True(query.Subparts[1].Properties.ContainsKey(QueryPropertyValue.NotNullable));

            Assert.Equal(QueryPartType.RuleDefinition, query.Subparts[2].Type);
            
            
            var rule = query.Subparts[2];
            Assert.Equal("uniqueIntStr", rule.Properties[QueryPropertyValue.RuleName]);
            var ruleTables = rule.Subparts;
            Assert.Equal(QueryPartType.ColumnName, ruleTables[0].Type);
            Assert.Equal(QueryPartType.ColumnName, ruleTables[1].Type);

            Assert.Equal("int", ruleTables[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("str", ruleTables[1].Properties[QueryPropertyValue.ColumnName]);
        }

        [Fact]
        public void ParseSelect()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM asd");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);

            Assert.Empty(from.Subparts);
            Assert.Equal("*", from.Properties[QueryPropertyValue.ColumnName]);


        }

        [Fact]
        public void ParseSelectWithColumnNames()
        {
            var query = QueryParser.ParseQuery("SELECT column, column2 FROM asd");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);

            Assert.Equal(2, from.Subparts.Count);

            var column = from.Subparts[0];
            Assert.Equal("column", column.Properties[QueryPropertyValue.ColumnName]);

            var column2 = from.Subparts[1];
            Assert.Equal("column2", column2.Properties[QueryPropertyValue.ColumnName]);


        }

        [Fact]
        public void ParseSelectWithColumnNameAndAlias()
        {
            var query = QueryParser.ParseQuery("SELECT column AS c FROM asd AS t1");

            Assert.Equal(QueryPartType.Select, query.Type);
            Assert.Single(query.Subparts);
            var from = query.Subparts.Single();

            Assert.Equal(QueryPartType.From, from.Type);
            Assert.Equal("asd", from.Properties[QueryPropertyValue.TableName]);
            Assert.Equal("t1", from.Properties[QueryPropertyValue.Alias]);

            Assert.Single(from.Subparts);

            var column = from.Subparts[0];
            Assert.Equal("column", column.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("c", column.Properties[QueryPropertyValue.Alias]);

        }

        [Fact]
        public void ParseSelectWithWhereColumnEqualsInteger()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE asd = 1");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var integer = statement.Subparts.Single();

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);

        }

        [Fact]
        public void ParseSelectWithWhereColumnEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE asd = asd2");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var column = statement.Subparts.Single();

            Assert.Equal(QueryPartType.ColumnName, column.Type);
            Assert.Equal("asd2", column.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = asd");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("1", statement.Properties[QueryPropertyValue.Integer]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereStringEqualsColumn()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE \"1\" = asd");

            Assert.Equal(2, query.Subparts.Count);
            var where = query.Subparts[1];
            Assert.Equal(QueryPartType.Where, where.Type);

            Assert.Single(where.Subparts);
            var statement = where.Subparts.Single();

            Assert.Equal(QueryPartType.Condition, statement.Type);
            Assert.Equal("1", statement.Properties[QueryPropertyValue.String]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", statement.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsInteger_ThrowsException()
        {
            Assert.Throws<SimpleRDMSException>(() => QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = 1"));
        }

        [Fact]
        public void ParseSelectWithWhereIntegerEqualsString_ThrowsException()
        {
            Assert.Throws<SimpleRDMSException>(() => QueryParser.ParseQuery("SELECT * FROM t1 WHERE 1 = \"1\""));
        }

        [Fact]
        public void ParseWhereWithAnd()
        {
            var query = QueryParser.ParseQuery("SELECT * FROM t1 WHERE \"1\" = asd AND asd = null OR 1 = asd");
            var where = query.Subparts[1];

            Assert.Equal(5, where.Subparts.Count);

            var first = where.Subparts[0];
            var second = where.Subparts[2];
            var third = where.Subparts[4];

            var and = where.Subparts[1];
            var or = where.Subparts[3];

            Assert.Equal(QueryPartType.Delimiter, and.Type);
            Assert.Equal(QueryPartType.Delimiter, or.Type);

            Assert.Equal("AND", and.Properties[QueryPropertyValue.LogicalConnectiveType]);
            Assert.Equal("OR", or.Properties[QueryPropertyValue.LogicalConnectiveType]);

            Assert.Equal("1", first.Properties[QueryPropertyValue.String]);
            Assert.Equal("=", first.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", first.Properties[QueryPropertyValue.ColumnName]);

            Assert.Equal(QueryPartType.Condition, second.Type);
            Assert.Equal("asd", second.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", second.Properties[QueryPropertyValue.Operator]);

            Assert.Single(second.Subparts);
            var nil = second.Subparts.Single();

            Assert.Equal(QueryPartType.Null, nil.Type);


            Assert.Equal(QueryPartType.Condition, third.Type);
            Assert.Equal("1", third.Properties[QueryPropertyValue.Integer]);
            Assert.Equal("=", third.Properties[QueryPropertyValue.Operator]);
            Assert.Equal("asd", third.Properties[QueryPropertyValue.ColumnName]);

        }

        [Fact]
        public void ParseInsertWithNull()
        {
            var query = QueryParser.ParseQuery("INSERT INTO t1 VALUES(1, \"asd\", null)");

            Assert.Equal(QueryPartType.Insert, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);

            Assert.Single(query.Subparts);

            var values = query.Subparts.Single();
            Assert.Equal(QueryPartType.InsertedValues, values.Type);

            Assert.Equal(3, values.Subparts.Count);

            var integer = values.Subparts[0];
            var str = values.Subparts[1];
            var nil = values.Subparts[2];

            Assert.Equal(QueryPartType.String, str.Type);
            Assert.Equal("asd", str.Properties[QueryPropertyValue.String]);

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);

            Assert.Equal(QueryPartType.Null, nil.Type);
            Assert.Equal("null", nil.Properties[QueryPropertyValue.Null]);
        }

        [Fact]
        public void ParseInsertWithSpecifiedColumns()
        {
            var query = QueryParser.ParseQuery("INSERT INTO t1 (intColumn, stringColumn) VALUES(1, \"asd\")");

            Assert.Equal(QueryPartType.Insert, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);

            Assert.Equal(2, query.Subparts.Count);

            var columns = query.Subparts[0];
            Assert.Equal(QueryPartType.InsertedColumns, columns.Type);

            Assert.Equal(2, columns.Subparts.Count);
            Assert.Equal("intColumn", columns.Subparts[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("stringColumn", columns.Subparts[1].Properties[QueryPropertyValue.ColumnName]);

            var values = query.Subparts[1];
            Assert.Equal(QueryPartType.InsertedValues, values.Type);

            Assert.Equal(2, values.Subparts.Count);

            var integer = values.Subparts[0];
            var str = values.Subparts[1];

            Assert.Equal(QueryPartType.String, str.Type);
            Assert.Equal("asd", str.Properties[QueryPropertyValue.String]);

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);


        }

        [Fact]
        public void ParseDelete()
        {
            var query = QueryParser.ParseQuery("DELETE FROM t1");

            Assert.Equal(QueryPartType.Delete, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);
            Assert.Empty(query.Subparts);
        }

        [Fact]
        public void ParseDeleteWithCondition()
        {
            var query = QueryParser.ParseQuery("DELETE FROM t1 WHERE name = \"asd\"");

            Assert.Equal(QueryPartType.Delete, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);
            Assert.Single(query.Subparts);

            var part = query.Subparts.Single();

            Assert.Equal(QueryPartType.Where, part.Type);

            var statement = part.Subparts.Single();

            Assert.Equal("name", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var column = statement.Subparts.Single();

            Assert.Equal(QueryPartType.String, column.Type);
            Assert.Equal("asd", column.Properties[QueryPropertyValue.String]);
        }
    }
}
