﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class InsertTests
    {
        [Fact]
        public void ParseInsertWithNull()
        {
            var query = QueryParser.ParseQuery("INSERT INTO t1 VALUES(1, \"asd\", null)");

            Assert.Equal(QueryPartType.Insert, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);

            Assert.Single(query.Subparts);

            var values = query.Subparts.Single();
            Assert.Equal(QueryPartType.InsertedValues, values.Type);

            Assert.Equal(3, values.Subparts.Count);

            var integer = values.Subparts[0];
            var str = values.Subparts[1];
            var nil = values.Subparts[2];

            Assert.Equal(QueryPartType.String, str.Type);
            Assert.Equal("asd", str.Properties[QueryPropertyValue.String]);

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);

            Assert.Equal(QueryPartType.Null, nil.Type);
            Assert.Equal("null", nil.Properties[QueryPropertyValue.Null]);
        }

        [Fact]
        public void ParseInsertWithSpecifiedColumns()
        {
            var query = QueryParser.ParseQuery("INSERT INTO t1 (intColumn, stringColumn) VALUES(1, \"asd\")");

            Assert.Equal(QueryPartType.Insert, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);

            Assert.Equal(2, query.Subparts.Count);

            var columns = query.Subparts[0];
            Assert.Equal(QueryPartType.InsertedColumns, columns.Type);

            Assert.Equal(2, columns.Subparts.Count);
            Assert.Equal("intColumn", columns.Subparts[0].Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("stringColumn", columns.Subparts[1].Properties[QueryPropertyValue.ColumnName]);

            var values = query.Subparts[1];
            Assert.Equal(QueryPartType.InsertedValues, values.Type);

            Assert.Equal(2, values.Subparts.Count);

            var integer = values.Subparts[0];
            var str = values.Subparts[1];

            Assert.Equal(QueryPartType.String, str.Type);
            Assert.Equal("asd", str.Properties[QueryPropertyValue.String]);

            Assert.Equal(QueryPartType.Integer, integer.Type);
            Assert.Equal("1", integer.Properties[QueryPropertyValue.Integer]);


        }

        [Fact]
        public void ParseInsertWithMultipleRows()
        {
            //TODO:
            //var query = QueryParser.ParseQuery("INSERT INTO t1 (intColumn, stringColumn) VALUES(1, \"asd\"), (2, \"asd2\")");
            //
            //Assert.Equal(QueryPartType.Insert, query.Type);
        }
    }
}
