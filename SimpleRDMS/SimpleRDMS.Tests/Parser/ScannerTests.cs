﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{

    public class ScannerTests
    {
        [Fact]
        public void ScannerTest1()
        {
            var scanner = new Scanner("SELECT w");

            var token = scanner.CurrentToken;
            scanner.MoveToNextToken();

            Assert.Equal("SELECT", token.Content);
            Assert.Equal(TokenType.Keyword, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();

            Assert.Equal("w", token.Content);
            Assert.Equal(TokenType.Symbol, token.Type);

            Assert.Equal(TokenType.EndOfString, scanner.CurrentToken.Type);
        }

        [Fact]
        public void ScannerTest2()
        {
            var scanner = new Scanner(" word  w  ");
            Assert.Equal("word", scanner.CurrentToken.Content);
            scanner.MoveToNextToken();
            Assert.Equal("w", scanner.CurrentToken.Content);
            scanner.MoveToNextToken();
            Assert.Equal(TokenType.EndOfString, scanner.CurrentToken.Type);
        }

        [Fact]
        public void ScannerTest3()
        {
            var scanner = new Scanner("CREATE TABLE t1(INTEGER int, STRING str)");

            var token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("CREATE", token.Content);
            Assert.Equal(TokenType.Keyword, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("TABLE", token.Content);
            Assert.Equal(TokenType.Keyword, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("t1", token.Content);
            Assert.Equal(TokenType.Symbol, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("(", token.Content);
            Assert.Equal(TokenType.SpecialCharacter, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("INTEGER", token.Content);
            Assert.Equal(TokenType.Keyword, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("int", token.Content);
            Assert.Equal(TokenType.Symbol, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal(",", token.Content);
            Assert.Equal(TokenType.SpecialCharacter, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("STRING", token.Content);
            Assert.Equal(TokenType.Keyword, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal("str", token.Content);
            Assert.Equal(TokenType.Symbol, token.Type);

            token = scanner.CurrentToken;
            scanner.MoveToNextToken();
            Assert.Equal(")", token.Content);
            Assert.Equal(TokenType.SpecialCharacter, token.Type);

            Assert.Equal(TokenType.EndOfString, scanner.CurrentToken.Type);
        }

        [Fact]
        public void ParseInt()
        {
            var scanner = new Scanner("1");

            Assert.Equal(TokenType.Integer, scanner.CurrentToken.Type);
            Assert.Equal("1", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseNotEqual()
        {
            var scanner = new Scanner("!=");

            Assert.Equal(TokenType.SpecialCharacter, scanner.CurrentToken.Type);
            Assert.Equal("!=", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseString()
        {
            var scanner = new Scanner("\"asd\"");

            Assert.Equal(TokenType.String, scanner.CurrentToken.Type);
            Assert.Equal("asd", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseSymbolWithSpecialCharacters()
        {
            var scanner = new Scanner("asd_asd.Asd");

            Assert.Equal(TokenType.Symbol, scanner.CurrentToken.Type);
            Assert.Equal("asd_asd.Asd", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseStringWithWhiteSpace()
        {
            var scanner = new Scanner("\"a sd\"");

            Assert.Equal(TokenType.String, scanner.CurrentToken.Type);
            Assert.Equal("a sd", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseNegativeNumber()
        {
            var scanner = new Scanner("-1");

            Assert.Equal(TokenType.Integer, scanner.CurrentToken.Type);
            Assert.Equal("-1", scanner.CurrentToken.Content);
        }

        [Fact]
        public void ParseBigNumber_ThrowsException()
        {
            Assert.Throws<SimpleRDMSException>(() => new Scanner("100000000000000000000000000000000000000"));
        }
    }
}
