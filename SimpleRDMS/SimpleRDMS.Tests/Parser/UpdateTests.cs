﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class UpdateTests
    {
        [Fact]
        public void ParseUpdateStatement()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = 1");

            Assert.Single(query.Subparts);
            var statement = query.Subparts.Single();
            Assert.Equal("int", statement.Properties[Compiler.Enums.QueryPropertyValue.ColumnName]);

            Assert.Single(statement.Subparts);
            Assert.Equal(QueryPartType.Integer, statement.Subparts.Single().Type);
            Assert.Equal("1", statement.Subparts.Single().Properties[Compiler.Enums.QueryPropertyValue.Integer]);
        }

        [Fact]
        public void ParseUpdateStatementWithMultipleValues()
        {
            var query = QueryParser.ParseQuery("UPDATE Test SET int = 1, str = \"1\"");


            Assert.Equal(2, query.Subparts.Count());
            var statement1 = query.Subparts.First();
            Assert.Equal("int", statement1.Properties[Compiler.Enums.QueryPropertyValue.ColumnName]);
            Assert.Single(statement1.Subparts);
            Assert.Equal(QueryPartType.Integer, statement1.Subparts.Single().Type);
            Assert.Equal("1", statement1.Subparts.Single().Properties[Compiler.Enums.QueryPropertyValue.Integer]);

            var statement2 = query.Subparts.Last();
            Assert.Equal("str", statement2.Properties[Compiler.Enums.QueryPropertyValue.ColumnName]);
            Assert.Single(statement2.Subparts);
            Assert.Equal(QueryPartType.String, statement2.Subparts.Single().Type);
            Assert.Equal("1", statement2.Subparts.Single().Properties[Compiler.Enums.QueryPropertyValue.String]);


        }
    }
}
