﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class AlterTableTests
    {
        /*       ALTER TABLE tablename ALTER COLUMN columnname SET NOT NULL;
                 ALTER TABLE tablename ALTER COLUMN columnname DROP NOT NULL;

                 ALTER TABLE tablename ADD COLUMN columnname datatype; 
                 ALTER TABLE tablename DROP COLUMN columnname; 

                 ALTER TABLE tablename ADD CONSTRAINT UCPerson UNIQUE (ID,LastName); 
                 ALTER TABLE tablename DROP CONSTRAINT UCPerson; */

        [Fact]
        public void SetColumnNotNullable()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename ALTER COLUMN columnname SET NOT NULL");

            Assert.Equal(QueryPartType.AlterTable, query.Type);
            Assert.Equal("tablename", query.Properties[QueryPropertyValue.TableName]);

            Assert.Single(query.Subparts);

            var notNullable = query.Subparts.Single();

            Assert.Equal(QueryPartType.AlterColumn, notNullable.Type);
            Assert.Equal("columnname", notNullable.Properties[QueryPropertyValue.ColumnName]);

            Assert.Equal(QueryPartType.SetNotNullable, notNullable.Subparts.Single().Type);
        }

        [Fact]
        public void DropColumnNotNullable()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename ALTER COLUMN columnname DROP NOT NULL");

            Assert.Single(query.Subparts);
           

            var alterColumn = query.Subparts.Single();

            Assert.Equal(QueryPartType.AlterColumn, alterColumn.Type);
            Assert.Equal("columnname", alterColumn.Properties[QueryPropertyValue.ColumnName]);

            Assert.Equal(QueryPartType.DropNotNullable, alterColumn.Subparts.Single().Type);
        }

        [Fact]
        public void AddColumn()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename ADD COLUMN columnname INTEGER");

            var subquery = query.Subparts.Single();
            Assert.Equal(QueryPartType.AddColumnOrConstraint, subquery.Type);

            var addedColumn = subquery.Subparts.Single();

            Assert.Equal(QueryPartType.ColumnDefinition, addedColumn.Type);
            Assert.Equal("columnname", addedColumn.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("INTEGER", addedColumn.Properties[QueryPropertyValue.DataType]);
        }

        [Fact]
        public void DropColumn()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename DROP COLUMN columnname");

            var subquery = query.Subparts.Single();
            Assert.Equal(QueryPartType.DropColumnOrRule, subquery.Type);
            var column = subquery.Subparts.Single();

            Assert.Equal(QueryPartType.DropColumn, column.Type);
            Assert.Equal("columnname", column.Properties[QueryPropertyValue.ColumnName]);
        }

        [Fact]
        public void AddConstraint()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename ADD CONSTRAINT UCPerson UNIQUE (ID,LastName)");

            var subquery = query.Subparts.Single();
            Assert.Equal(QueryPartType.AddColumnOrConstraint, subquery.Type);

            var rule = subquery.Subparts.Single();

            Assert.Equal(QueryPartType.RuleDefinition, rule.Type);
            Assert.Equal("UNIQUE", rule.Properties[QueryPropertyValue.RuleType]);
            Assert.Equal("UCPerson", rule.Properties[QueryPropertyValue.RuleName]);
            
        }

        [Fact]
        public void DropConstraint()
        {
            var query = QueryParser.ParseQuery("ALTER TABLE tablename DROP CONSTRAINT UCPerson");

            var subquery = query.Subparts.Single();
            Assert.Equal(QueryPartType.DropColumnOrRule, subquery.Type);
            var column = subquery.Subparts.Single();

            Assert.Equal(QueryPartType.DropRule, column.Type);
            Assert.Equal("UCPerson", column.Properties[QueryPropertyValue.RuleName]);
        }
    }
}
