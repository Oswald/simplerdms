﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class DeleteTests
    {
        [Fact]
        public void ParseDelete()
        {
            var query = QueryParser.ParseQuery("DELETE FROM t1");

            Assert.Equal(QueryPartType.Delete, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);
            Assert.Empty(query.Subparts);
        }

        [Fact]
        public void ParseDeleteWithCondition()
        {
            var query = QueryParser.ParseQuery("DELETE FROM t1 WHERE name = \"asd\"");

            Assert.Equal(QueryPartType.Delete, query.Type);
            Assert.Equal("t1", query.Properties[QueryPropertyValue.TableName]);
            Assert.Single(query.Subparts);

            var part = query.Subparts.Single();

            Assert.Equal(QueryPartType.Where, part.Type);

            var statement = part.Subparts.Single();

            Assert.Equal("name", statement.Properties[QueryPropertyValue.ColumnName]);
            Assert.Equal("=", statement.Properties[QueryPropertyValue.Operator]);

            Assert.Single(statement.Subparts);
            var column = statement.Subparts.Single();

            Assert.Equal(QueryPartType.String, column.Type);
            Assert.Equal("asd", column.Properties[QueryPropertyValue.String]);
        }
    }
}
