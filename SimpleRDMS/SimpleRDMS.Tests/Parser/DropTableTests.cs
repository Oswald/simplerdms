﻿using SimpleRDMS.Compiler.Enums;
using SimpleRDMS.Compiler.Logic;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Parser
{
    public class DropTableTests
    {
        [Fact]
        public void DropTable()
        {
            var query = QueryParser.ParseQuery("DROP TABLE test");

            Assert.Equal(QueryPartType.DropTable, query.Type);
            Assert.Equal("test", query.Properties[QueryPropertyValue.TableName]);
        }
    }
}
