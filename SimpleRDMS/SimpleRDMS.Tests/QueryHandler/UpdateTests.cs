﻿using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.queryRunner
{
    public class UpdateTests
    {
        [Fact]
        public void UpdateWithoutCondition_UpdatesAllRows()
        {
            var table = TestEntities.GetTableWithOneColumn();
            var update = new UpdateHandler(new Core.Entities.UpdateProperties(table, new Dictionary<string, object> { ["int"] = 3 }));
            
            update.UpdateRows(new Stack<SimpleRDMS.Buffer.Results.ResultBase>());

            Assert.Equal(2, table.Count);
            Assert.Equal(2, table.ToList().Count());
            Assert.True(table.All(row => row.Values[0].Equals(3)));
        }

        [Fact]
        public void UpdateWithCondition_UpdatesRow()
        {
            var table = TestEntities.GetTableWithOneColumn();
            var update = new UpdateHandler(new Core.Entities.UpdateProperties(table, new Dictionary<string, object> { ["int"] = 3 }, 
                new Core.Entities.WhereConnective(new Core.Entities.WhereComparison(Core.Entities.ValueType.ColumnName, "int", Core.Entities.ValueType.Constant, 2, Core.Entities.WhereFilterType.Equal))
            ));

            update.UpdateRows(new Stack<SimpleRDMS.Buffer.Results.ResultBase>());

            Assert.Equal(2, table.Count);
            Assert.Equal(2, table.ToList().Count());
            Assert.DoesNotContain(table, row => row.Values[0].Equals(2));
            Assert.Contains(table, row => row.Values[0].Equals(1));
            Assert.Contains(table, row => row.Values[0].Equals(3));
        }
    }
}
