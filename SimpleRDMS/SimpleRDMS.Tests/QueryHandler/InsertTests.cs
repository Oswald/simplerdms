﻿using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.queryRunner
{
    public class InsertTests
    {
        [Fact]
        public void InsertValue_InsertsValue()
        {
            var table = TestEntities.GetTableWithOneColumn();
            var insert = new InsertHandler(new Core.Entities.InsertProperties(table, new List<object[]> { new object[] { 3 } }));
            
            insert.InsertRows(new Stack<SimpleRDMS.Buffer.Results.ResultBase>());

            Assert.Contains(table, row => row.Values[0].Equals(3));
            Assert.Equal(3, table.Count);
        }
    }
}
