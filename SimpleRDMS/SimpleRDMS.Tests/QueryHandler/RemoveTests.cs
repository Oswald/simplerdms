﻿using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.queryRunner
{
    public class RemoveTests
    {
        [Fact]
        public void RemoveAllFromTable_TableIsEmpty()
        {
            var table = TestEntities.GetTableWithOneColumn();
            var delete = new DeleteHandler(new DeleteProperties(table));

            delete.RemoveRows(new Stack<SimpleRDMS.Buffer.Results.ResultBase>());
            Assert.Empty(table);
        }

        [Fact]
        public void RemoveAllFromTableWithCondition_TableContainsOneRow()
        {
            var table = TestEntities.GetTableWithOneColumn();
            var delete = new DeleteHandler(new DeleteProperties(table, new WhereConnective(new WhereComparison(Core.Entities.ValueType.ColumnName, "int", Core.Entities.ValueType.Constant, 1, WhereFilterType.Equal))));

            delete.RemoveRows(new Stack<SimpleRDMS.Buffer.Results.ResultBase>());
            Assert.Single(table);
            Assert.Equal(2, table.Single().Values[0]);
        }
    }
}
