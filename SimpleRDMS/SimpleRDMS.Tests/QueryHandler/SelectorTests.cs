﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Core.QuerySets;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.queryRunner
{
    public class SelectorTests
    {
        [Fact]
        public void SelectAllFromEmptyTable()
        {
            var table = new Table();
            table.Name = "Test";
            table.Columns = new Column[]
            {
                new Column { Name = "int", Nullable = true, Type = DataType.Integer  }
            };

            var rows = new SelectHandler(new List<SelectProperties> { new SelectProperties("Test", table, new ColumnProperties[] { new ColumnProperties { ColumnName = "int", ColumnAlias = "int" } } )}).ToList();

            Assert.Empty(rows);

        }

        [Fact]
        public void SelectColumnThatDoesNotExist()
        {
            var table = new Table();
            table.Name = "Test";
            table.Columns = new Column[]
            {
                new Column { Name = "int", Nullable = true, Type = DataType.Integer  }
            };
            table.Insert(new object[] { 1 });

            var handler = new SelectHandler(new List<SelectProperties> { new SelectProperties("Test", table, new ColumnProperties[] { new ColumnProperties { ColumnName = "str", ColumnAlias = "str" } }) });

            Assert.Throws<SimpleRDMSException>(() => handler.ToList());

        }

        [Fact]
        public void SelectAllFromSingleTable_ReturnsRows()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions() }).ToList();

            Assert.Equal(new object[] { 1 }, selector[0].Values);
            Assert.Equal(new object[] { 2 }, selector[1].Values);
            Assert.Equal(2, selector.Count());
            Assert.Single(selector.First().Values);
            Assert.Single(selector.Last().Values);
        }

        [Fact]
        public void SelectTwoColumnsFromThreeColumnTable_LengthIsCorrect()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptionsForTwoColumnsInThreeColumnsTable() }).ToList();

            Assert.Equal(2, selector[0].Values.Length);
            Assert.Equal(1, selector[0]["int1"]);
            Assert.Equal(1, selector[0]["int3"]);
        }

        [Fact]
        public void SelectTwoColumnsFromThreeColumnTable_ValuesAreCorrect()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptionsForTwoColumnsInThreeColumnsTable() }).ToList();


            Assert.Equal(1, selector[1]["int1"]);
            Assert.Equal(3, selector[1]["int3"]);
        }


        [Fact]
        public void SelectAllFromTwoTables_ReturnsRows()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() }).ToList();

            Assert.Equal(new object[] { 1, 1 }, selector[0].Values);
            Assert.Equal(new object[] { 2, 1 }, selector[1].Values);
            Assert.Equal(new object[] { 1, 2 }, selector[2].Values);
            Assert.Equal(new object[] { 2, 2 }, selector[3].Values);

            Assert.Equal(4, selector.Count());
            Assert.Equal(2, selector.First().Values.Count());
        }

        [Fact]
        public void SelectAllFromSingleTable_CanAccessValueWithColumnName()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions() });

            int i = 1;
            foreach (var row in selector)
            {
                Assert.Equal(i, row["int"]);
                i += 1;
            }
        }

        [Fact]
        public void SelectAllFromSingleTable_CanAccessValueWithTableAndColumnName()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions() });

            int i = 1;
            foreach (var row in selector)
            {
                Assert.Equal(i, row["Test.int"]);
                i += 1;
            }
        }

        [Fact]
        public void SelectAllFromTwoTables_CannotAccessValueWithColumnName()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() }).ToList();

            foreach (var row in selector)
            {
                Assert.Throws<SimpleRDMSException>(() => row["int"]);
            }
        }

        [Fact]
        public void SelectAllFromTwoTables_CannotAccessValueWithTableAndColumnName()
        {
            var selector = new SelectHandler(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() }).ToList();

            foreach (var row in selector)
            {
                Assert.Equal(1, row["Test1.int"]);
                break;
            }
        }
    }
}
