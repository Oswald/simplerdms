﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Tests.queryRunner
{
    static class TestEntities
    {
        public static Table GetTableWithOneColumn()
        {
            var table = new Table();
            table.Name = "Test";
            table.Columns = new Column[]
            {
                new Column { Name = "int", Nullable = true, Type = DataType.Integer  }
            };

            table.Insert(
                new object[]
                {
                    1
                    
                }
            );

            table.Insert(
                new object[]
                {
                    2
                }
            );

            return table;
        }

        public static Table GetTableWithThreeColumns()
        {
            var table = new Table();
            table.Name = "Test";
            table.Columns = new Column[]
            {
                new Column { Name = "int1", Nullable = true, Type = DataType.Integer  },
                new Column { Name = "int2", Nullable = true, Type = DataType.Integer  },
                new Column { Name = "int3", Nullable = true, Type = DataType.Integer  }
            };

            table.Insert(
                new object[]
                {
                    1,
                    1,
                    1
                }
            );

            table.Insert(
                new object[]
                {
                    1,
                    2,
                    3
                }
            );

            table.Insert(
                new object[]
                {
                    1,
                    3,
                    1,
                }
            );



            return table;
        }

        public static SelectProperties GetOptions()
        {
            return new SelectProperties(GetTableWithOneColumn().Name, GetTableWithOneColumn(), new ColumnProperties[] { new ColumnProperties { ColumnName = "int", ColumnAlias = "int" } } );
        }

        public static SelectProperties GetOptionsForTwoColumnsInThreeColumnsTable()
        {
            return new SelectProperties(GetTableWithThreeColumns().Name, GetTableWithThreeColumns(), new ColumnProperties[] 
            { 
                new ColumnProperties { ColumnName = "int1", ColumnAlias = "int1" },
                new ColumnProperties { ColumnName = "int3", ColumnAlias = "int3" }
            });
        }

        public static SelectProperties GetOptionsForAllColumnsInThreeColumnsTable()
        {
            return new SelectProperties(GetTableWithOneColumn().Name, GetTableWithOneColumn(), new ColumnProperties[]
            {
                new ColumnProperties { ColumnName = "*", ColumnAlias = "*" },
            });
        }

        public static SelectProperties GetOptionsForAllColumnsInTwoColumnsTable()
        {
            return new SelectProperties(GetTableWithOneColumn().Name, GetTableWithOneColumn(), new ColumnProperties[] { new ColumnProperties { ColumnName = "*" } });
        }

        public static SelectProperties GetOptionsForThreeColumnTable()
        {
            return new SelectProperties(GetTableWithThreeColumns().Name, GetTableWithThreeColumns(), new ColumnProperties[] 
            { 
                new ColumnProperties
                {
                    ColumnAlias = "int1",
                    ColumnName = "int1"
                },
                new ColumnProperties
                {
                    ColumnAlias = "int2",
                    ColumnName = "int2"
                },
                new ColumnProperties
                {
                    ColumnAlias = "int3",
                    ColumnName = "int3"
                }
            });
        }
    }
}
