﻿using SimpleRDMS.Core.Entities;
using SimpleRDMS.Core.QueryEnumerators;
using SimpleRDMS.Core.QueryHandlers;
using SimpleRDMS.Core.QueryRunners;
using SimpleRDMS.Core.QuerySets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.queryRunner
{
    public class WhereTests
    {
        [Fact]
        public void SelectFromTableWhereValueIsConstant_ReturnsRow()
        {
            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptions() });
            var where = new WhereHandler(selector, new WhereConnective(new WhereComparison(Core.Entities.ValueType.Constant, 1, Core.Entities.ValueType.ColumnName, "int", WhereFilterType.Equal))).ToList();

            Assert.Single(where);
            Assert.Equal(1, where.Single().Values.Single());
        }

        [Fact]
        public void SelectFromTwoTablesWhereValueIsValue_ReturnsRow()
        {
            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() });
            var where = new WhereHandler(selector, new WhereConnective(new WhereComparison(Core.Entities.ValueType.ColumnName, "Test1.int", Core.Entities.ValueType.ColumnName, "Test2.int", WhereFilterType.Equal))).ToList();

            Assert.True(where.All(w => w.Values[0].Equals(w.Values[1])));
        }

        [Fact]
        public void SelectFromTwoTablesWithAndCondition_ReturnsRow()
        {
            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() });

            var filter1 = new WhereComparison(Core.Entities.ValueType.ColumnName, "Test1.int", Core.Entities.ValueType.ColumnName, "Test2.int", WhereFilterType.Equal);
            var filter2 = new WhereComparison(Core.Entities.ValueType.ColumnName, "Test1.int", Core.Entities.ValueType.Constant, 1, WhereFilterType.NotEqual);
            var properties = new WhereConnective(filter1, WhereStatementType.AND, new WhereConnective(filter2));

            var where = new WhereHandler(selector, properties).ToList();

            Assert.Single(where);
            Assert.Equal(2, where.Single().Values[0]);
            Assert.Equal(2, where.Single().Values[1]);
        }

        [Fact]
        public void SelectFromTwoTablesWithOrCondition_ReturnsRow()
        {
            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptions(), TestEntities.GetOptions() });

            var filter1 = new WhereComparison(Core.Entities.ValueType.ColumnName, "Test1.int", Core.Entities.ValueType.ColumnName, "Test2.int", WhereFilterType.Equal);
            var filter2 = new WhereComparison(Core.Entities.ValueType.ColumnName, "Test1.int", Core.Entities.ValueType.Constant, 1, WhereFilterType.NotEqual);
            var properties = new WhereConnective(filter1, WhereStatementType.OR, new WhereConnective(filter2));

            var where = new WhereHandler(selector, properties).ToList();

            Assert.Equal(3, where.Count);
            Assert.False(where.All(w => w.Values[0] == w.Values[1] || w.Values[0].Equals(2)));
        }

        [Fact]
        public void SelectWithComplexAndCondition()
        {
            var statement1 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int2", WhereFilterType.Equal);
            var statement2 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int3", WhereFilterType.Equal);

            var where = new WhereConnective(statement1, WhereStatementType.AND, statement2);

            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptionsForThreeColumnTable() });
            var results = new WhereHandler(selector, where).ToList();

            Assert.Single(results);
            Assert.Contains(results, row => row.Values.All(v => v.Equals(1)));

        }

        [Fact]
        public void SelectWithComplexOrCondition()
        {
            var statement1 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int2", WhereFilterType.Equal);
            var statement2 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int3", WhereFilterType.Equal);

            var where = new WhereConnective(statement1, WhereStatementType.OR, statement2);

            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptionsForThreeColumnTable() });
            var results = new WhereHandler(selector, where).ToList();

            Assert.Equal(2, results.Count);

            Assert.Contains(results, row => row.Values.All(v => v.Equals(1)));
            Assert.Contains(results, row => row.Values[0].Equals(1) && row.Values[1].Equals(3) && row.Values[2].Equals(1));
        }

        [Fact]
        public void ParseConditionWithBrackets()
        {
            var statement1 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int2", WhereFilterType.Equal);
            var statement2 = new WhereComparison(Core.Entities.ValueType.ColumnName, "int1", Core.Entities.ValueType.ColumnName, "int3", WhereFilterType.Equal);

            var where = new WhereConnective(statement1, WhereStatementType.OR, statement2);

            var selector = new SelectEnumerator(new List<SelectProperties> { TestEntities.GetOptionsForThreeColumnTable() });
            var results = new WhereHandler(selector, where).ToList();

            Assert.Equal(2, results.Count);

            Assert.Contains(results, row => row.Values.All(v => v.Equals(1)));
            Assert.Contains(results, row => row.Values[0].Equals(1) && row.Values[1].Equals(3) && row.Values[2].Equals(1));
        }
    }
}
