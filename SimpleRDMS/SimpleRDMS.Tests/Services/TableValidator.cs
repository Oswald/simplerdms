﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Tests.Buffer;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Services
{
    static class TableValidator
    {
        //Validates content and rules
        public static void AreEqual(Table t1, Table t2)
        {
            BufferValidator.AreEqual(BufferValidator.ValidateTable(t1), BufferValidator.ValidateTable(t2));

            Assert.Equal(t1.Columns.Length, t2.Columns.Length);

            for(int i = 0; i < t1.Columns.Length; i++)
            {
                Assert.Equal(t1.Columns[i].Type, t2.Columns[i].Type);
                Assert.Equal(t1.Columns[i].Name, t2.Columns[i].Name);
                Assert.Equal(t1.Columns[i].Nullable, t2.Columns[i].Nullable);
            }

            Assert.Equal(t1.UniqueConstraints.Count, t2.UniqueConstraints.Count);

            for(int i = 0; i < t1.UniqueConstraints.Count; i++)
            {
                Assert.Equal(t1.UniqueConstraints[i].UniqueColumns, t2.UniqueConstraints[i].UniqueColumns);
            }
        }
    }
}
