﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Compiler.Entities;
using SimpleRDMS.Tests.Buffer;
using Xunit;
using SimpleRDMS.Compiler.Logic;
using SimpleRDMS.Compiler.Enums;

namespace SimpleRDMS.Tests.Services
{
    public class TableServiceTests
    {
        /*
        [Fact]
        public void CreateTable_CreatesTable()
        {
            var tableService = new TableService();
            var table = tableService.CreateTable(TestEntities.GetPropertiesForTwoColumnTable(), new List<object[]>());

            table.Insert(new object[]
            {
                1,
                "1"
            });

            Assert.Equal(TestEntities.GetPropertiesForTwoColumnTable().Name, table.Name);
        }

        [Fact]
        public void CreateTableWithUniqueRule_CreatesTable()
        {
            var tableService = new TableService();
            var table = tableService.CreateTable(TestEntities.GetPropertiesWithRules(), new List<object[]>());

            table.Insert(new object[]
            {
                1,
                "1"
            });

            Assert.Throws<SimpleRDMSException>(() => table.Insert(new object[]
            {
                1,
                "1"
            }));
        }

        [Fact]
        public void CreateTableWithNotNull_CreatesTable()
        {
            var tableService = new TableService();
            var table = tableService.CreateTable(TestEntities.GetPropertiesWithRules(), new List<object[]>());

            table.Insert(new object[]
            {
                1,
                "1"
            });

            Assert.Throws<SimpleRDMSException>(() => table.Insert(new object[]
            {
                null,
                1
            }));
        }

        [Fact]
        public void CreateTableWithContent_CreatesTable()
        {
            var tableService = new TableService();
            var table = tableService.CreateTable(TestEntities.GetPropertiesForTwoColumnTable(), new List<object[]> { new object[]
            {
                1,
                "1"
            }});

            var table2 = Buffer.TestEntities.GetTableWithTwoColumns();

            table2.Insert(new object[]
            {
                1,
                "1"
            });

            var content1 = BufferValidator.ValidateTable(table);
            var content2 = BufferValidator.ValidateTable(table2);

            BufferValidator.AreEqual(content1, content2);

            Assert.Equal(TestEntities.GetPropertiesForTwoColumnTable().Name, table.Name);
        }

        [Fact]
        public void StringifyTableAndParseString()
        {
            var tableService = new TableService();
            var t1 = Buffer.TestEntities.GetTableWithTwoColumns();
            t1.UniqueConstraints.Add(new UniqueConstraintRule(t1, new string[] { "int" }));

            var createString = tableService.GetTablePropertiesString(t1);
            var query = QueryParser.ParseQuery(createString);

            Assert.Equal(QueryPartType.CreateTable, query.Type);
            var properties = ((CreateTableQuery)query).TableProperties;

            Assert.Equal(t1.Name, properties.Name);
            Assert.Equal(t1.Columns[0].Name, properties.Rules.Single(r => r.Name == SimpleRDMS.Compiler.Enums.TableRuleType.Unique).Columns.Single());
            Assert.Equal(t1.Columns[1].Name, properties.Rules.Single(r => r.Name == SimpleRDMS.Compiler.Enums.TableRuleType.NotNull).Columns.Single());
        }
        */
    }
}
