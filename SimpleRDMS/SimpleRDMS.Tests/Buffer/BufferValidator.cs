﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    static class BufferValidator
    {
        /// <summary>
        /// Validates the table and returns array containing the values
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static object[,] ValidateTable(Table table)
        {
            var rows = new object[table.Count, table.Columns.Length];
            var row = table.First;
            for(int i = 0; i < table.Count; i ++)
            {
                for(int j = 0; j < table.Columns.Length; j++)
                {
                    rows[i, j] = row.Values[j];
                }
                row = row.Next;
            }

            if(row != null)
            {
                throw new Exception("Row.Next should have been null for the last element, but it was not");
            }

            //Iterate backwards and validate links
            row = table.Last;
            for (int i = 0; i < table.Count; i++)
            {
                for (int j = 0; j < table.Columns.Length; j++)
                {
                    Assert.Equal(rows[table.Count - 1 - i, j], row.Values[j]);
                }
                row = row.Previous;
            }

            if (row != null)
            {
                throw new Exception("Row.Previous should have been null for the first element, but it was not");
            }

            return rows;
        }

        public static void AreEqual(object[,] first, object[,] second)
        {
            for(int i = 0; i < first.GetLength(0); i++)
            {
                for (int j = 0; j < first.GetLength(1); j++)
                {
                    Assert.Equal(first[i, j], second[i, j]);
                }
            }
        }
    }
}
