﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Buffer.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleRDMS.Tests.Buffer
{
    static class TestEntities
    {
        /// <summary>
        /// Columns: nullable int, string
        /// </summary>
        /// <returns></returns>
        public static Table GetTableWithTwoColumns()
        {
            var table = new Table();
            table.Name = "Test";
            table.Columns = new Column[] 
            { 
                new Column { Name = "int", Nullable = true, Type = DataType.Integer  }, 
                new Column { Name = "str", Nullable = false, Type = DataType.String } 
            };

            return table;
        }

        public static Table GetTableWithTwoColumnsAndUniqueConstraint()
        {
            var table = GetTableWithTwoColumns();

            table.CreateUniqueConstraint("int_unique", new string[] { "int"});

            return table;
        }

        static object[] GetRow(int? intValue, string stringValue)
        {
            return new object[]
            {
                intValue,
                stringValue
            };
        }

        public static List<object[]> GetFiveRows()
        {
            var results = new List<object[]>();
            results.Add(GetRow(1, "1"));
            results.Add(GetRow(2, "2"));
            results.Add(GetRow(3, "3"));
            results.Add(GetRow(4, "4"));
            results.Add(GetRow(5, "5"));

            return results;
        }


    }
}
