﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    public class BufferUpdateTests
    {
        [Fact]
        public void UpdateRow_UpdatesRow()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.Insert(
                new object[]
                {
                    1,
                    "1"
                }
            );

            var newValue = new object[]
            {
                    -1,
                    "-1"
            };

            table.Update(table.Last, newValue);

            Assert.Equal((int)newValue[0], (int)table.First.Values[0]);
            Assert.Equal((string)newValue[1], (string)table.First.Values[1]);
            Assert.Equal(1, table.Count);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void UpdateRowWithUniqueConstraint_UpdatesRow()
        {
            var table = TestEntities.GetTableWithTwoColumnsAndUniqueConstraint();
            table.Insert(
                new object[]
                {
                    1,
                    "1"
                }
            );

            var newValue = new object[]
           {
                    1,
                    "-1"
           };

           table.Update(table.Last, newValue);

            Assert.Equal((int)newValue[0], (int)table.First.Values[0]);
            Assert.Equal((string)newValue[1], (string)table.First.Values[1]);
            Assert.Equal(1, table.Count);
            BufferValidator.ValidateTable(table);
        }
    }
}
