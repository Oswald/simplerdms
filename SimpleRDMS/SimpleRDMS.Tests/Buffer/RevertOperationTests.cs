﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    public class RevertOperationTests
    {
        [Fact]
        public void InsertFiveRowsAndRevertOne_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();


            table.Insert(rows[0]);
            table.Insert(rows[1]);
            table.Insert(rows[2]);
            table.Insert(rows[3]);

            var state = BufferValidator.ValidateTable(table);

            var result = table.Insert(rows[4]);
            result.Revert();
            var stateAfterRevert = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(state, stateAfterRevert);

        }

        [Fact]
        public void InsertFiveRowsAndRevertTwo_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();


            table.Insert(rows[0]);
            table.Insert(rows[1]);
            table.Insert(rows[2]);


            var state = BufferValidator.ValidateTable(table);

            var result1 = table.Insert(rows[3]);
            var result2 = table.Insert(rows[4]);
            result2.Revert();
            result1.Revert();
            var stateAfterRevert = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(state, stateAfterRevert);

        }

        [Fact]
        public void RemoveInMiddleAndRevert_TableIsValid()
        {
            var modelTable = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();


            modelTable.Insert(rows[0]);
            modelTable.Insert(rows[1]);
            modelTable.Insert(rows[3]);
            modelTable.Insert(rows[4]);

            var modelState = BufferValidator.ValidateTable(modelTable);

            var table = TestEntities.GetTableWithTwoColumns();

            table.Insert(rows[0]);
            table.Insert(rows[1]);
            var result = table.Insert(rows[2]);
            table.Insert(rows[3]);
            table.Insert(rows[4]);

            result.Revert();

            var state = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(modelState, state);

        }

        [Fact]
        public void RemoveTwoInMiddleAndRevert_TableIsValid()
        {
            var modelTable = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();


            modelTable.Insert(rows[0]);
            modelTable.Insert(rows[1]);
            modelTable.Insert(rows[4]);

            var modelState = BufferValidator.ValidateTable(modelTable);

            var table = TestEntities.GetTableWithTwoColumns();

            table.Insert(rows[0]);
            table.Insert(rows[1]);
            var result1 = table.Insert(rows[2]);
            var result2 = table.Insert(rows[3]);
            table.Insert(rows[4]);

            result2.Revert();
            result1.Revert();

            var state = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(modelState, state);

        }

        [Fact]
        public void RevertAfterInsertUpdateAndDelete_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();

            foreach (var row in rows)
            {
                table.Insert(row);
            }

            var modelState = BufferValidator.ValidateTable(table);

            var insertedRow = new object[]
            {
               6,
               "6"
            };

            var result1 = table.Remove(table.First);
            var result2 = table.Update(table.First, new object[] { 1, "1" });
            var result3 = table.Insert(insertedRow);

            Assert.Equal(5, table.Count);

            result3.Revert();
            result2.Revert();
            result1.Revert();

            var stateAfterRevert = BufferValidator.ValidateTable(table);
            BufferValidator.AreEqual(modelState, stateAfterRevert);
        }

        [Fact]
        public void RevertAfterAddingColumn_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();

            foreach (var row in rows)
            {
                table.Insert(row);
            }

            var initialState = BufferValidator.ValidateTable(table);

            var result = table.CreateColumn(new Column { Name = "integer1", Nullable = false, Type = DataType.Integer });
            result.Revert();

            var stateAfterRevert = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(initialState, stateAfterRevert);
        }

        [Fact]
        public void RevertAfterRemovingColumn_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();

            foreach (var row in rows)
            {
                table.Insert(row);
            }

            var initialState = BufferValidator.ValidateTable(table);

            var result = table.RemoveColumn("int");
            result.Revert();

            var stateAfterRevert = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(initialState, stateAfterRevert);
        }

        [Fact]
        public void RevertAfterInsertUpdateDeleteAndRemoveRowAndAddRow_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();

            foreach (var row in rows)
            {
                table.Insert(row);
            }

            var modelState = BufferValidator.ValidateTable(table);

            var insertedRow = new object[]
            {
               "6"
            };

            var result1 = table.Remove(table.First);
            var result2 = table.RemoveColumn("int");
            var result3 = table.Update(table.First, new object[] { "1" });
            var result4 = table.Insert(insertedRow);
            var result5 = table.CreateColumn(new Column { Name = "newint", Nullable = false, Type = DataType.Integer });
            var result6 = table.Insert(new object[] { "7", 7 });

            Assert.Equal(6, table.Count);

            result6.Revert();
            result5.Revert();
            result4.Revert();
            result3.Revert();
            result2.Revert();
            result1.Revert();

            var stateAfterRevert = BufferValidator.ValidateTable(table);
            BufferValidator.AreEqual(modelState, stateAfterRevert);
        }
    }
}
