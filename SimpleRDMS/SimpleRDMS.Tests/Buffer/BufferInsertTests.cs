﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Buffer.Enums;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    public class BufferInsertTests
    {
        [Fact]
        public void InsertRow_InsertsRow()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.Insert(
                new object[]
                {
                    1,
                    "1"!
                }
            );

            Assert.Equal(1, (int)table.First.Values[0]);
            Assert.Equal("1", (string)table.First.Values[1]);
            Assert.Equal(1, table.Count);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertNull_InsertsRow()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.Insert(
                new object[]
                {
                    null,
                    "1"
                }
            );

            Assert.Null(table.First.Values[0]);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertNullWhenNotNullable_ThrowsException()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var row =
                new object[]
                {
                    1,
                    null
                };

            Assert.Throws<SimpleRDMSException>(() => table.Insert(row));
        }

        [Fact]
        public void InsertRowWithWrongLength_ThrowsException()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var row =
                new object[]
                {
                    1
                };

            Assert.Throws<SimpleRDMSException>(() => table.Insert(row));
        }

        [Fact]
        public void InsertMultipleWhenUnique_ThrowsException()
        {
            var table = TestEntities.GetTableWithTwoColumns();

            table.UniqueConstraints.Add(new UniqueConstraintRule(table, "c", new string[] { "int", "str" }));
            table.Insert(
                new object[]
                {
                    1,
                    "1"
                }
            );

            Assert.Throws<SimpleRDMSException>(() => table.Insert(new object[]
                {
                    1,
                    "1"
                }));

            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertRowWithWrongTypes_ThrowsException()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var row =
                new object[]
                {
                    1,
                    1
                };

            Assert.Throws<SimpleRDMSException>(() => table.Insert(row));
        }

        [Fact]
        public void InsertMultipleRows_InsertsRows()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.Insert(
                new object[]
                {
                    1,
                    "1"
                }
            );

            var secondRow = new object[]
            {
                1,
                "test2"                
            };

            table.Insert(secondRow);

            Assert.Equal("1", (string)table.First.Values[1]);
            Assert.Equal("test2", (string)table.Last.Values[1]);
            Assert.Equal(2, table.Count);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertFiveRows_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();

            foreach(var row in rows)
            {
                table.Insert(row);
            }

            BufferValidator.ValidateTable(table);

        }
    }
}
