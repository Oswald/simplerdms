﻿using SimpleRDMS.Buffer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    public class BufferRemoveTests
    {
        [Fact]
        public void RemoveRow_TableIsEmpty()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.Insert(
                new object[]
                {
                    1,
                    "1"
                }
            );


            table.Remove(table.Last);

            Assert.Equal(0, table.Count);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertFiveRowsAndRemoveOne_TableIsValid()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            var rows = TestEntities.GetFiveRows();


            table.Insert(rows[0]);
            table.Insert(rows[1]);
            table.Insert(rows[2]);
            table.Insert(rows[3]);

            var state = BufferValidator.ValidateTable(table);

            table.Insert(rows[4]);
            table.Remove(table.Last);
            var stateAfterRevert = BufferValidator.ValidateTable(table);

            BufferValidator.AreEqual(state, stateAfterRevert);

        }
    }
}
