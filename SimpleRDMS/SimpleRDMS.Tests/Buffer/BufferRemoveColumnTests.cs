﻿using SimpleRDMS.Buffer.Entities;
using SimpleRDMS.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SimpleRDMS.Tests.Buffer
{
    public class BufferRemoveColumnTests
    {
        [Fact]
        public void InsertRowAfterRemovingRow_InsertsRow()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.RemoveColumn("int");
            table.Insert(
                new object[]
                {
                    "1"
                }
            );

            Assert.Equal("1", (string)table.First.Values[0]);
            Assert.Equal(1, table.Count);
            BufferValidator.ValidateTable(table);
        }

        [Fact]
        public void InsertRowAfterRemovingUniqueColumn_ThrowsException()
        {
            var table = TestEntities.GetTableWithTwoColumns();
            table.UniqueConstraints.Add(new UniqueConstraintRule(table, "c", new string[] { "str" }));
            table.RemoveColumn("str");
            
            Assert.Throws<SimpleRDMSException>(() =>
            table.Insert(
                new object[]
                {
                    1
                }
            ));
        }
    }
}
