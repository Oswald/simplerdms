﻿using SimpleRDMS;
using System;
using System.IO;
using System.Linq;

namespace ClientApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Data");
                var connection = new DatabaseConnection(path);

                Console.WriteLine("Database loaded successfylly.\r\n");

                string line;

                while((line = Console.ReadLine()) != "quit")
                {
                    var result = connection.ExecuteQuery(line);

                    if (result.Success)
                    {
                        if (result.Results == null)
                        {
                            Console.WriteLine("Query executed successfully");
                        }
                        else if(result.Results.Count() == 0)
                        {
                            Console.WriteLine("Query yielded no results");
                        }
                        else
                        {
                            foreach(var column in result.Results.First().Columns)
                            {
                                Console.Write("{0, -20}", column);
                            }
                            Console.WriteLine();
                            Console.WriteLine();

                            foreach (var row in result.Results)
                            {
                                string value = "";
                                foreach (var cell in row.Values)
                                {
                                    if (cell == null)
                                    {
                                        value = "NULL";
                                    }
                                    else if (cell is int)
                                    {
                                        value = cell.ToString();
                                    }
                                    else
                                    {
                                        value = $"\"{cell}\"";
                                    }

                                    Console.Write("{0, -20}", value);
                                }

                                Console.WriteLine();
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Error: {result.Exception.Message}");
                    }

                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fatal error:\r\n {e.ToString()}");
            }
            Console.ReadLine();
        }
    }
}
